<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('room_id');
            $table->string('code');
            $table->string('name');
            $table->enum('condition', ['Baik', 'Rusak']);
            $table->text('desc')->nullable();
            $table->unsignedInteger('qty')->default(0);
            $table->text('picture');
            $table->text('slug');
            $table->timestampsTz();

            $table->index(['user_id', 'type_id', 'room_id']);
            $table->foreign('user_id')->on('users')->references('id');
            $table->foreign('type_id')->on('types')->references('id');
            $table->foreign('room_id')->on('rooms')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
