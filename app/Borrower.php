<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Borrow;

class Borrower extends Model
{
    protected $fillable = ['user_id', 'nip', 'name', 'phone', 'address'];

    /**
     * Merelasikan dengan model user
     *
     * @return void
     */
    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Merelasikan dengan model borrow
     *
     * @return void
     */
    public function borrow ()
    {
        return $this->hasMany(Borrow::class);
    }
}
