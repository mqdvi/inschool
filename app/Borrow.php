<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Borrower;
use App\Inventory;
use App\BorrowDetail;

class Borrow extends Model
{
    protected $fillable = ['borrower_id', 'borrow_at', 'return_at'];

    /**
     * Untuk merelasikan dengan Model::borrower
     *
     * @return void
     */
    public function borrower ()
    {
        return $this->belongsTo(Borrower::class);
    }

    /**
     * Untuk merelasikan dengan model::Inventory
     *
     * @return void
     */
    public function inventory ()
    {
        return $this->belongToMany(Inventory::class);
    }

    /**
     * Untuk merelasikan dengan Model:borrowDetail
     *
     * @return void
     */
    public function borrowDetail () 
    {
        return $this->hasMany(BorrowDetail::class);
    }
}
