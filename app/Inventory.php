<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Type;
use App\Room;
use App\Borrow;
use App\BorrowDetail;
use App\User;

class Inventory extends Model
{
    protected $fillable = ['user_id', 'type_id', 'room_id', 'code', 'name', 'condition', 'desc', 'qty', 'picture'];

    /**
     * Merelasikan dengan model type
     *
     * @return void
     */
    public function type () 
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * Merelasikan dengan odel room
     *
     * @return void
     */
    public function room ()
    {
        return $this->belongsTo(Room::class);
    }

    public function user ()
    {
        return $this->belongsTo(User::class);
    }

    public function borrow ()
    {
        return $this->belongsToMany(Borrow::class);
    }

    public function borrowDetail () 
    {
        return $this->hasMany(BorrowDetail::class);
    }

    public function setNameAttribute ($name)
    {
        $this->attributes['name'] = $name;
        $this->attributes['slug'] = str_slug($name);
    }

    public function getUrlAttribute ()
    {
        if(Auth::user()->level_id === 3) {
            return route('admin.invent.edit_invent', $this->slug);
        } else {
            return route('operator.invent.edit_invent', $this->slug);
        }
    }
}
