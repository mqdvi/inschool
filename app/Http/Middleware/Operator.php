<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Operator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->level_id == 3) {
                return redirect()->route('admin.index');
            } elseif (Auth::user()->level_id == 1) {
                return redirect()->route('peminjam.index');
            }
        } else {
            return redirect()->route('auth.login');
        }

        return $next($request);
    }
}
