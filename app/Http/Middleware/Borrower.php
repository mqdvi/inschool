<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Borrower
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if(Auth::user()->level_id == 2) {
                return redirect()->route('operator.index');
            } elseif (Auth::user()->level_id == 3) {
                return redirect()->route('admin.index');
            }
        } else {
            return redirect()->route('auth.login');
        }

        return $next($request);
    }
}
