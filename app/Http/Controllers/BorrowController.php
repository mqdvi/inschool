<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

use App\Borrow as Borrows;
use App\Borrower;
use App\BorrowDetail;
use App\User;
use App\Inventory;

use Carbon\Carbon;

class BorrowController extends Controller
{
    /**
     * Menampilkan data list peminjaman
     *
     * @return void
     */
    public function index()
    {
        return view('borrow/index');
    }

    /**
     * Menampilkan data permintaan peminjaman
     *
     * @return void
     */
    public function request()
    {
        return view('borrow/request');
    }

    /**
     * Menampilkan halaman create peminjaman
     *
     * @return void
     */
    public function create()
    {
        $borrower = Borrower::all();
        return view('borrow/create', compact('borrower'));
    }

    /**
     * Menampilkan halaman edit peminjaman
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $data = Borrows::find($id);
        $status = BorrowDetail::where('borrow_id', $id)->select('status')->first()->toArray();
        $data->status = $status;
        return view('borrow/edit', compact('data'));
    }

    /**
     * Membuat data untuk ditampilkan kedalam table di halaman daftar peminjaman
     *
     * @return void
     */
    public function list()
    {
        $borrow = DB::table('detail_peminjaman')->get();

        $data = array();
        foreach($borrow as $item) {
            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act .= '<i class="fa fa-ellipsis-h"></i>';
            $act .= '</a>';
            $act .= '<div class="dropdown-menu" style="width:50px;">';
            $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="showData('.$item->id.')">Detail</a>';
            $act .= '<a class="dropdown-item" href="'.route("borrow.edit", $item->id).'">Edit</a>';
            $act .= '</div>';
            $item->action = $act;         
            $item->borrow_at = date('d F Y', strtotime($item->borrow_at));

            if($item->picture) {
                $item->picture = '<img src="'.asset($item->picture).'" class="img-thumb">';
            } else {
                $item->picture = '<img src="'.asset('img/no-photo.png').'" class="img-thumb">';
            }
            
            if($item->status == "Pending") {
                $item->status = '<span class="badge badge-pill badge-primary">'.$item->status.'</span>';
            } elseif($item->status == "Dipinjam") {
                $item->status = '<span class="badge badge-pill badge-info">'.$item->status.'</span>';
            } else {
                $item->status = '<span class="badge badge-pill badge-danger">'.$item->status.'</span>';
            }

            $data[] = $item;
        }
        // dd($data);
        echo json_encode(array("sEcho" => 1, "aaData" => $data));
    }

    /**
     * Mengolah data untuk ditampilkan kedalam table di halaman permintaan peminjaman
     *
     * @return void
     */
    public function list_request()
    {
        $borrow = DB::table('detail_peminjaman')->where('status', 'Pending')->get();

        $data = array();
        foreach($borrow as $item) {
            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act .= '<i class="fa fa-ellipsis-h"></i>';
            $act .= '</a>';
            $act .= '<div class="dropdown-menu" style="width:50px;">';
            $act .= '<a class="dropdown-item" href="'.route('borrow.update_request', [$item->id, 'Terima']).'">Terima</a>';
            $act .= '<a class="dropdown-item" href="'.route('borrow.update_request', [$item->id, 'Tolak']).'">Tolak</a>';
            $act .= '</div>';
            $item->action = $act;         
            $item->borrow_at = date('d F Y', strtotime($item->borrow_at));

            if($item->picture) {
                $item->picture = '<img src="'.asset($item->picture).'" class="img-thumb">';
            } else {
                $item->picture = '<img src="'.asset('img/no-photo.png').'" class="img-thumb">';
            }
            
            $item->status = '<span class="badge badge-pill badge-primary">'.$item->status.'</span>';

            $data[] = $item;
        }
        // dd($data);
        echo json_encode(array("sEcho" => 1, "aaData" => $data));
    }

    /**
     * API untuk mendapatkan kode barang berdasarkan {type_id}
     *
     * @param [type] $code
     * @return void
     */
    public function api($code)
    {
        $invent = Inventory::where('code', $code)->first();
        $invent->types = $invent->type->name;
        $invent->rooms = $invent->room->name;
        echo json_encode($invent);
    }

    /**
     * Untuk membuat data peminjaman ke database
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'qty' => 'gt:0'
        ], [
            'code.required' => 'Kode barang harus diisi',
            'qty.gt' => 'Jumlah harus lebih dari 0'
        ]);

        Borrows::create([
            'borrower_id' => $request->borrower_id,
            'borrow_at' => date('Y-m-d H:i:s', strtotime($request->borrow_at))
        ])->borrowDetail()->create([
            'inventory_id' => $request->invent_id,
            'qty' => $request->qty,
            'status' => $request->status
        ]);

        activity()
            ->withProperties([
                'obj' => date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah membuat peminjaman baru pada ');

        return redirect()->route('borrow.index')->with([
            'success' => 'Selamat! Anda berhasil menambahkan peminjaman baru!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk mengupdate data peminjaman
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $borrow = Borrows::find($id);
        $borrow->borrowDetail()->update(['status' => $request->status]);

        if($request->return_at) {
            $borrow->return_at = date('Y-m-d H:i:s', strtotime($request->return_at));
        }
        
        $borrow->update();

        activity()
            ->withProperties([
                'obj' => $borrow->id.' pada '.date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah mengedit peminjaman dengan ID : ');

        return redirect()->route('borrow.edit', $id)->with([
            'success' => 'Selamat! Anda berhasil mengedit peminjaman!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk mengupdate/menerima permintaan peminjaman
     *
     * @param [type] $id
     * @param [type] $param
     * @return void
     */
    public function update_request($id, $param)
    {
        $borrow = Borrows::find($id);
        
        if($param === "Terima") {
            $borrow->borrowDetail()->update(['status' => 'Dipinjam']);
        } else {
            $borrow->borrowDetail()->update(['status' => 'Ditolak']);
        }
        $borrow->update();

        activity()
            ->withProperties([
                'obj' => $borrow->id.' pada '.date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah mengedit peminjaman dengan ID : ');

        return redirect()->route('borrow.request')->with([
            'success' => 'Selamat! Anda berhasil mengedit peminjaman!',
            'status' => TRUE
        ]);
    }
}
