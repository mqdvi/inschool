<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

use App\Borrow;
use App\Borrower;
use App\BorrowDetail;
use App\Inventory;
use App\User;

use Carbon\Carbon;

class OperatorController extends Controller
{
    /**
     * Untuk menampilkan halaman dashboard operator
     *
     * @return void
     */
    public function index()
    {
        $data = array(
            'goods' => Inventory::all()->count(),
            'borrow' => BorrowDetail::where('status', 'Dipinjam')->count(),
            'borrower' => Borrower::all()->count(),
            'request' => BorrowDetail::where('status', 'Pending')->count()
        );

        $user = DB::table('new_users')->get();
        $newUser = array();

        foreach($user as $users) {
            $newUser[] = $users;
        }

        $good = DB::table('inventories')
                ->orderBy('created_at', 'desc')
                ->limit(5)
                ->get();
        $newGoods = array();

        foreach($good as $item) {
            $newGoods[] = $item;
        }

        return view('operator/index', compact('data', 'newUser', 'newGoods'));
    }

    /**
     * Untuk mengolah data peminjaman dan ditampilkan dalam chart
     *
     * @return void
     */
    public function chart()
    {
        $data = Borrow::get(['id', 'borrow_at'])
                ->groupBy(function ($d) {
                    return Carbon:: parse($d -> borrow_at) -> format('m');
                });
        $collection = array();
        $setData = array();

        $nol = "0";
        for ($i = 1; $i <= 12; $i++) {
            if ($i > 9) {
                $nol = "";
            }

            if (!@$data[$nol.$i]) {
                $collection[$i] = array();
            } else {
                $collection[$i] = $data[$nol.$i];
            }
        }

        foreach($collection as $item) {
            $setData[] = count($item);
        }

        return $setData;
    }
}
