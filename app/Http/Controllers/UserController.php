<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

use App\User as Users;
use App\Admin;
use App\Borrower;
use App\Operator;

class UserController extends Controller
{
    /**
     * Menampilkan list data pengguna
     *
     * @return void
     */
    public function index()
    {
        return view('user/index');
    }

    /**
     * Menampilkan halaman permintaan peminjam
     *
     * @return void
     */
    public function request()
    {
        return view('user/request');
    }

    /**
     * Menampilkan halaman create pengguna
     *
     * @return void
     */
    public function create()
    {
        return view('user/create');
    }

    /**
     * Menampilkan halaman edit pengguna
     *
     * @param [type] $id
     * @return void
     */
    public function edit($id)
    {
        $data = Users::findOrFail($id);

        if($data->level_id == 1) {
            $user = Borrower::findOrFail($data->borrower['id']);
        } elseif($data->level_id == 2) {
            $user = Operator::findOrFail($data->operator['id']);
        } elseif($data->level_id == 3) {
            $user = Admin::findOrFail($data->admin['id']);
        }

        return view('user/create', compact('data', 'user'));
    }

    /**
     * Mengolah data list pengguna untuk ditabmpikan kedalam table
     *
     * @return void
     */
    public function list()
    {
        $user = Users::where('approved', 1)->get();

        $data = array();
        foreach($user as $item) {
            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act .= '<i class="fa fa-ellipsis-h"></i>';
            $act .= '</a>';
            $act .= '<div class="dropdown-menu" style="width:50px;">';
            $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="showData('.$item->id.')">Detail</a>';
            $act .= '<a class="dropdown-item" href="'.route("admin.user.edit", $item->id).'">Edit</a>';
            
            if($item->level_id !== 3) {
                if(Auth::user()->level_id === 3) {
                    $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData('.$item->id.')">Delete</a>';
                }
            }
            
            $act .= '</div>';
            $item['action'] = $act;
            $item['picture'] = '<img src="'.asset($item->picture ?? 'img/no-photo.png').'" class="img-thumb">';
            
            
            if($item->level_id == 1) {
                $item['level'] = '<span class="badge badge-pill badge-primary">Peminjam</span>';
                $item['name'] = $item->borrower['name'];
                $item['nip'] = $item->borrower['nip'] ?? '-';
                $item['phone'] = $item->borrower['phone'];
            } elseif ($item->level_id == 2) {
                $item['level'] = '<span class="badge badge-pill badge-info">Operator</span>';
                $item['name'] = $item->operator['name'];
                $item['nip'] = $item->operator['nip'] ?? '-';
                $item['phone'] = '-';
            } else {
                $item['level'] = '<span class="badge badge-pill badge-danger">Admin</span>';
                $item['name'] = $item->admin['name'];
                $item['nip'] = $item->admin['nip'] ?? '-';
                $item['phone'] = '-';
            }

            $data[] = $item;
        }
        echo json_encode(array("sEcho" => 1, "aaData" => $data));    
    }

    /**
     * Untuk menampilkan list data permintaan pengguna
     *
     * @return void
     */
    public function list_request()
    {
        $user = Users::where(['approved' => 0, 'level_id' => 1])->get();

        $data = array();
        foreach($user as $item) {
            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act .= '<i class="fa fa-ellipsis-h"></i>';
            $act .= '</a>';
            $act .= '<div class="dropdown-menu" style="width:50px;">';
            $act .= '<a class="dropdown-item" href="'.route('admin.user.update_request', $item->id).'">Terima</a>';
            $act .= '</div>';
            $item['action'] = $act;
            $item['picture'] = '<img src="'.asset($item->picture ?? 'img/no-photo.png').'" class="img-thumb">';
            $item['status'] = '<span class="badge badge-pill badge-primary">Pending</span>';

            if($item->level_id == 1) {
                $item['level'] = '<span class="badge badge-pill badge-primary">Peminjam</span>';
                $item['name'] = $item->borrower['name'];
                $item['nip'] = $item->borrower['nip'] ?? '-';
                $item['phone'] = $item->borrower['phone'];
            } elseif ($item->level_id == 2) {
                $item['level'] = '<span class="badge badge-pill badge-info">Operator</span>';
                $item['name'] = $item->operator['name'];
                $item['nip'] = $item->operator['nip'] ?? '-';
                $item['phone'] = '-';
            } else {
                $item['level'] = '<span class="badge badge-pill badge-danger">Admin</span>';
                $item['name'] = $item->admin['name'];
                $item['nip'] = $item->admin['nip'] ?? '-';
                $item['phone'] = '-';
            }

            $data[] = $item;
        }
        echo json_encode(array("sEcho" => 1, "aaData" => $data));    
    }

    /**
     * Men-save data pengguna kedalam database
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'picture' => 'nullable|image|max:10000',
            'username' => 'required|unique:users|min:6',
            'password' => 'required|min:6'
        );

        if($request->level_id == 1) {
            $rules['phone'] = 'required|min:11|unique:borrowers';
        }

        if($request->password){
            $rules['password'] = 'min:6';
        }

        if($request->nip){
            $rules['nip'] = 'min:18';
        }
        
        $request->validate($rules, [
            'username.required' => 'Username harus diisi',
            'username.unique' => 'Username sudah digunakan',
            'username.min' => 'Username minimal memiliki :min karakter',
            'password.required' => 'Password harus diisi',
            'password.min' => 'Password minimal memiliki :min karakter',
            'name.required' => 'Nama harus diisi',
            'password.min' => 'Password minimal memiliki :min karakter',
            'picture.image' => 'Format file tidak didukung',
            'picture.max' => 'File terlalu besar',
            'phone.required' => 'No telp harus diisi',
            'phone.min' => 'No telp minimal memiliki :min karakter',
            'phone.max' => 'No telp maksimal memiliki :max karakter',
            'phone.unique' => 'No telp telah digunakan',
            'address.required' => 'Alamat harus diisi',
            'nip.min' => 'NIP minimal memiliki :min karakter'
        ]);
        
        $data = [
            'level_id' => $request->level_id,
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'approved' => $request->approved
        ];

        if($request->picture) {
            $str = str_slug($request->name);
            $picture = $request->file('picture');
            $imgName = $str.'.'.$picture->getClientOriginalExtension();
            $path = public_path('img\profile/');
            $request->file('picture')->move($path, $imgName);

            $data['picture'] = 'img/profile/'.$imgName;
        }

        $user = Users::create($data);

        if($request->level_id == 1) {
            $user->borrower()->create([
                'nip' => $request->nip,
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address
            ]);
        } elseif ($request->level_id == 2) {
            $user->operator()->create([
                'nip' => $request->nip,
                'name' => $request->name,
            ]);
        } elseif ($request->level_id == 3) {
            $user->admin()->create([
                'name' => $request->name
            ]);
        }

        activity()
            ->withProperties([
                'obj' => $request->name.' dengan username '.$request->username,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah membuat Akun baru yaitu :');

        return redirect()->route('admin.user.create')->with([
            'success' => 'Selamat! Anda berhasil memasukkan Akun baru!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk mengupdate data pengguna
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required'
        );

        if($request->level_id == 1) {
            $rules['phone'] = 'required|min:11';
        }

        if($request->password){
            $rules['password'] = 'min:6';
        }

        if($request->nip){
            $rules['nip'] = 'min:18';
        }

        if($request->picture) {
            $rules['picture'] = 'nullable|image|max:10000';
        }
        
        $request->validate($rules, [
            'password.min' => 'Password minimal memiliki :min karakter',
            'name.required' => 'Nama harus diisi',
            'picture.image' => 'Format file tidak didukung',
            'picture.max' => 'File terlalu besar',
            'phone.required' => 'No telp harus diisi',
            'phone.min' => 'No telp minimal memiliki :min karakter',
            'nip.min' => 'NIP minimal memiliki :min karakter'
        ]);

        $user = Users::findOrFail($id);

        Users::where('id',$id)->update([
            'approved' => $request->approved
        ]);
        
        if($request->password) {
            $user->password = bcrypt($request->password);
        }

        if($request->picture) {
            $str = str_slug($request->name);
            $picture = $request->file('picture');
            $imgName = $str.'.'.$picture->getClientOriginalExtension();
            $path = public_path('img\profile/');

            if(File::exists($user->picture)){
                File::delete($user->picture);
            }

            $request->file('picture')->move($path,$imgName);
            $user->picture = 'img\profile/'.$imgName;
        }

        if($request->level_id == 1) {
            $user->borrower()->update([
                'nip' => $request->nip,
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address
            ]);
        } elseif ($request->level_id == 2) {
            $user->operator()->update([
                'nip' => $request->nip,
                'name' => $request->name,
            ]);
        } elseif ($request->level_id == 3) {
            $user->admin()->update([
                'name' => $request->name
            ]);
        }

        activity()
            ->withProperties([
                'obj' => $request->name.' dengan username '.$request->username,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah mengedit Akun yaitu :');

        return redirect()->route('admin.user.create')->with([
            'success' => 'Selamat! Anda berhasil mengedit Akun!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk mengupdate/menerima data pengguna (peminjam)
     *
     * @param [type] $id
     * @return void
     */
    public function update_request($id)
    {
        $user = Users::find($id);
        $user->approved = 1;        
        $user->update();

        activity()
            ->withProperties([
                'obj' => $user->username.' pada '.date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah mengkonfirmasi pengguna dengan username : ');

        return redirect()->route('admin.user.request')->with([
            'success' => 'Selamat! Anda berhasil mengkonfirmasi pengguna!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk menghapus pengguna
     *
     * @param [type] $id
     * @return void
     */
    public function destroy($id)
    {
        $user = Users::findOrFail($id);
        
        if($user->level_id == 1) {
            $client = Borrower::where('user_id',$user->borrower['user_id'])->delete();
        } elseif ($user->level_id == 2) {
            $client = Operator::where('user_id',$user->operator['user_id'])->delete();
        }

        File::delete($user->picture);
        $user->delete();

        activity()
            ->withProperties([
                'obj' => $user->username.' pada '.date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah menghapus pengguna yaitu : ');

        return response()->json([
            'success' => TRUE,
            'message' => 'Anda berhasil menghapus pengguna!'
        ]);
    }
}
