<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Validator;
use App\User;
use App\Http\Requests\Login;
use App\Http\Requests\Register;

class AuthController extends Controller
{
    /**
     * Menampilkan halaman login
     *
     * @return void
     */
    public function login () {
        return view('auth/login');
    }
    
    /**
     * Menampilkan halaman register
     *
     * @return void
     */
    public function register () {
        return view('auth/register');
    }

    /**
     * Untuk menyimpan/register data peminjam
     *
     * @param Register $request
     * @return void
     */
    public function save (Register $request) {
        $user = User::create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'level_id' => 1 
        ])->borrower()->create([
            'nip' => $request->nip,
            'name' => $request->name,
            'phone' => $request->phone,
            'address' => $request->address
         ]);

        return redirect()->route('auth.register')->with([
            'msg' => 'Silahkan hubungi admin atau operator',
            'status' => true
        ]);
    }

    /**
     * Fungsi untuk login
     *
     * @param Login $request
     * @return void
     */
    public function submit (Login $request) { 
        if(Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            Session::put('id', Auth::user()->id);
            Session::put('username', Auth::user()->username);
            Session::put('role', Auth::user()->level_id);

            $ip = gethostbyname(gethostname());

            if(Auth::user()->approved){
                if(Auth::user()->level_id == 1){
                    Session::put('name', Auth::user()->borrower->name);
                    Session::put('as', 'peminjam');

                    activity()
                        ->withProperties([
                            'obj' => 'sistem dengan IP '.$ip,
                            'by' => '<b>'.Auth::user()->borrower->name.'</b>'
                        ])
                        ->log('telah melakukan login pada');

                    return redirect()->route('peminjam.index');
                } elseif(Auth::user()->level_id == 2){
                    Session::put('name', Auth::user()->operator->name);
                    Session::put('as', 'operator');

                    $user = User::findOrFail(Auth::user()->id);

                    if($user->operator->ip_address == null) {
                        $user->operator()->update([
                            'ip_address' => $ip
                        ]);

                        activity()
                        ->withProperties([
                            'obj' => 'sistem dengan IP '.$ip,
                            'by' => '<b>'.Auth::user()->operator->name.'</b>'
                        ])
                        ->log('telah melakukan login pada');

                        return redirect()->route('operator.index');
                    } elseif ($user->operator->ip_address !== $ip) {
                        Session::flush();
                        Auth::logout();
                     
                        $msg = "Anda tidak berada di device yang sama!";
                        
                        return view('auth.login', compact('msg'));
                    } else {
                        return redirect()->route('operator.index');
                    }
                } elseif (Auth::user()->level_id == 3){
                    Session::put('name', Auth::user()->admin->name);
                    Session::put('as', 'admin');
                    
                    activity()
                        ->withProperties([
                            'obj' => 'sistem dengan IP '.$ip,
                            'by' => '<b>'.Auth::user()->admin->name.'</b>'
                        ])
                        ->log('telah melakukan login pada');
                    
                    return redirect()->route('admin.index');
                }
            } else {
                $msg = "Silahkan hubungi admin atau operator untuk konfirmasi akun anda";
                return view('auth.login', compact('msg'));
            }
        } else {
            $msg = "Username atau password salah";
            return view('auth.login', compact('msg'));
        }
    }

    /**
     * Fungsi untuk logout
     *
     * @return void
     */
    public function logout () {
        Auth::logout();
        return redirect()->route('auth.login');
    }
}
