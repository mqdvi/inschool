<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

use App\Borrow;
use App\Borrower;
use App\BorrowDetail;
use App\Inventory;
use App\User;

use Carbon\Carbon;

class AdminController extends Controller
{
    /**
    *   Untuk menampilkan halaman dashboard Admin
    */
    public function index()
    {
        // Membuat data jumlah untuk didashboard
        $data = array(
            'goods' => Inventory::all()->count(),
            'borrow' => BorrowDetail::where('status', 'Dipinjam')->count(),
            'borrower' => Borrower::all()->count(),
            'request' => BorrowDetail::where('status', 'Pending')->count()
        );

        // Memanggil view new_users untuk menampilkan peminjam terbaru
        $user = DB::table('new_users')->get();
        $newUser = array();

        foreach($user as $users) {
            $newUser[] = $users;
        }

        // Mengambil data barang terbaru 
        $good = DB::table('inventories')
                ->orderBy('created_at', 'desc')
                ->limit(5)
                ->get();
        $newGoods = array();

        foreach($good as $item) {
            $newGoods[] = $item;
        }

        return view('admin/index', compact('data', 'newUser', 'newGoods'));
    }

    /**
     * Untuk mengolah data untuk ditampilkan kedalam chart
     *
     * @return void
     */
    public function chart()
    {
        // Mengambil data peminjaman berdasarkan bulan dan menghitungnya 
        $data = Borrow::get(['id', 'borrow_at'])
                ->groupBy(function ($d) {
                    return Carbon:: parse($d -> borrow_at) -> format('m');
                });
        $collection = array();
        $setData = array();

        $nol = "0";
        for ($i = 1; $i <= 12; $i++) {
            if ($i > 9) {
                $nol = "";
            }

            if (!@$data[$nol.$i]) {
                $collection[$i] = array();
            } else {
                $collection[$i] = $data[$nol.$i];
            }
        }

        foreach($collection as $item) {
            $setData[] = count($item);
        }

        return $setData;
    }

    /**
     * Untuk membackup Database
     *
     * @return void
     */
    public function backup()
    {
        $host = 'localhost';
        $user = 'root';
        $pass = '';
        $dbname = 'db_inschool';
        
        $backup_file = $dbname . date("dmY-His") . '.sql';
        $command = "mysqldump --opt -h $host -u $user --single-transaction ". "db_inschool > $backup_file";

        // Menjalankan perintah query mysqldump
        system($command, $error);
        
        if($error) {
            $msg = "Maaf, gagal untuk membackup database.";
            $err = true;
        } else {
            $msg = "Anda berhasil untuk membackup database!";
            $err = false;

            activity()
                ->withProperties([
                    'obj' => $db_inschool.' pada '.date('H:i'),
                    'by' => '<b>'.Auth::user()->admin->name.'</b>'
                ])
                ->log('telah membackup database ');
        }

        echo json_encode(array(
            'msg' => $msg,
            'err' => $err
        ));
    }

}
