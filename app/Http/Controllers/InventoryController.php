<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoomType;
use App\Http\Requests\Inventory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

use App\Room;
use App\Type;
use App\Inventory as Inventories;
use App\User;
use App\BorrowDetail;
use App\Borrow;

use Validator;

use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;

class InventoryController extends Controller
{
    /**
     * untuk menampilkan halaman list data barang
     *
     * @return void
     */
    public function index()
    {
        return view('inventory/index');
    }

    /**
     * untuk menampilkan halaman list data ruangan
     *
     * @return void
     */
    public function room()
    {
        return view('inventory/room');
    }

    /**
     * untuk menampilkan halaman list data tipe barang
     *
     * @return void
     */
    public function type()
    {
        return view('inventory/type');
    }

    /**
     * untuk mengambil list data barang
     *
     * @return void
     */
    public function list_invent()
    {
        $invent = Inventories::all();

        $data = array();
        foreach($invent as $item) {
            $room = Room::findOrFail($item->room_id);

            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act.= '<i class="fa fa-ellipsis-h"></i>';
            $act.= '</a>';
            $act.= '<div class="dropdown-menu" style="width:50px;">';
            $act.= '<a class="dropdown-item" href="javascript:void(0)" onclick="showData('.$item->id.')">Detail</a>';
            $act.= '<a class="dropdown-item" href="javascript:void(0)" onclick="printLabel(\''.$item->code.'\')">Print Label</a>';
            $act.= '<a class="dropdown-item" href="'.route("invent.edit_invent", $item->id).'">Edit</a>';
            $act.= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData('.$item->id.')">Delete</a>';
            $act.= '</div>';
            $item['action'] = $act;
            $item['picture'] = '<img src="'.asset($item->picture).'" class="img-thumb">';
            $item['room_id'] = $room->name;
            
            if($item->condition == "Baik") {
                $item['condition'] = '<span class="badge badge-pill badge-primary">'.$item->condition.'</span>';
            } else {
                $item['condition'] = '<span class="badge badge-pill badge-danger">'.$item->condition.'</span>';
            }

            $data[] = $item;
        }
        echo json_encode(array("sEcho" => 1, "aaData" => $data));
    }

    /**
     * mengambil/mengolah list data ruangan untuk ditampilkan ke table list ruangan
     *
     * @return void
     */
    public function list_room()
    {
        $room = Room::all();

        $data = array();
        foreach($room as $item) {
            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act .= '<i class="fa fa-ellipsis-h"></i>';
            $act .= '</a>';
            $act .= '<div class="dropdown-menu" style="width:50px;">';
            if(Auth::user()->level_id === 3) {
                $act .= '<a class="dropdown-item" href="'.route("invent.edit_room", $item->id).'">Edit</a>';
                $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData('.$item->id.')">Delete</a>';
            }
            $act .= '</div>';
            $item['action'] = $act;
            $data[] = $item;
        }

        echo json_encode(array("sEcho" => 1, "aaData" => $data));
    }

    /**
     * mengambil/mengolah list data ruangan untuk ditampilkan ke table list ruangan
     *
     * @return void
     */
    public function list_type()
    {
        $type = Type::all();

        $data = array();
        foreach($type as $item) {
            $act = '<a href="#" class="non-caret dropdown-toggle" data-toggle="dropdown">';
            $act .= '<i class="fa fa-ellipsis-h"></i>';
            $act .= '</a>';
            $act .= '<div class="dropdown-menu" style="width:50px;">';
            if(Auth::user()->level_id === 3) {
                $act .= '<a class="dropdown-item" href="'.route("invent.edit_type", $item->id).'">Edit</a>';
                $act .= '<a class="dropdown-item" href="javascript:void(0)" onclick="deleteData('.$item->id.')">Delete</a>';
            }
            $act .= '</div>';
            $item['action'] = $act;
            $data[] = $item;
        }

        echo json_encode(array("sEcho" => 1, "aaData" => $data));
    }

    /**
     * Menampilkan halaman create barang
     *
     * @return void
     */
    public function create_invent()
    {
        $room = Room::all();
        $type = Type::all();

        return view('inventory/create__invent', compact('room', 'type'));
    }

    /**
     * Menampilkan halaman create ruangan
     *
     * @return void
     */
    public function create_room()
    {
        return view('inventory/create__room');
    }

    /**
     * Menampilkan halaman create tipe barang
     *
     * @return void
     */
    public function create_type()
    {
        return view('inventory/create__type');
    }

    /**
     * Menampilkan halaman edit barang
     *
     * @param [type] $slug
     * @return void
     */
    public function edit_invent($slug)
    {
        $data = Inventories::findOrFail($slug);
        $room = Room::all();
        $type = Type::all();
        
        return view('inventory/create__invent', compact('data', 'room', 'type'));
    }

    /**
     * Menampilkan halaman edit ruangan
     *
     * @param [type] $code
     * @return void
     */
    public function edit_room($code)
    {
        $data = Room::findOrFail($code);
        return view('inventory/create__room', compact('data'));
    }

    /**
     * Menampilkan halaman edit tipe barang
     *
     * @param [type] $code
     * @return void
     */
    public function edit_type($code)
    {
        $data = Type::findOrFail($code);
        return view('inventory/create__type', compact('data'));
    }

    /**
     * Untuk men-save data barang ke database
     *
     * @param Inventory $request
     * @return void
     */
    public function store_invent(Inventory $request)
    {
        //slug
        $str = str_slug($request->name);

        //image process
        $img = $request->file('picture');
        $imgName = $str.'.'.$img->getClientOriginalExtension();
        $path = public_path('img\inventory/');
        $request->file('picture')->move($path, $imgName);

        Inventories::create([
            'user_id' => Auth::user()->id,
            'type_id' => $request->type_id,
            'room_id' => $request->room_id,
            'code' => $request->code,
            'name' => $request->name,
            'condition' => $request->condition,
            'desc' => $request->desc,
            'qty' => $request->qty,
            'picture' => 'img/inventory/'.$imgName
        ]);

        activity()
            ->withProperties([
                'obj' => $request->name.' pada '.date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah membuat barang baru yaitu : ');

        return redirect()->route('invent.create_invent')->with([
            'success' => 'Selamat! Anda berhasil menambahkan barang baru!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk men-save data ruangan ke database
     *
     * @param Request $request
     * @return void
     */
    public function store_room(Request $request)
    {   
        $request->validate([
            'code' => 'required|unique:rooms|alpha_num',
            'name' => 'required',
            'desc' => 'required'
        ], [
            'code.alpha_num' => 'Kode harus diisi dengan valid',
            'code.required' => 'Kode harus diisi',
            'code.unique' => 'Kode sudah digunakan',
            'name.required' => 'Nama harus diisi',
            'desc.required' => 'Deskripsi harus diisi'
        ]);

        Room::create($request->all());

        activity()
            ->withProperties([
                'obj' => $request->name.' dengan kode '.$request->code,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah membuat ruangan baru yaitu :');

        return redirect()->route('invent.create_room')->with([
            'success' => 'Selamat! Anda berhasil memasukkan ruangan baru!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk men-save data tipe barang ke database
     *
     * @param Request $request
     * @return void
     */
    public function store_type(Request $request)
    {   
        $request->validate([
            'code' => 'required|unique:types|alpha_num',
            'name' => 'required',
            'desc' => 'required'
        ], [
            'code.alpha_num' => 'Kode harus diisi dengan valid',
            'code.required' => 'Kode harus diisi',
            'code.unique' => 'Kode sudah digunakan',
            'name.required' => 'Nama harus diisi',
            'desc.required' => 'Deskripsi harus diisi'
        ]);

        Type::create($request->all());

        activity()
            ->withProperties([
                'obj' => $request->name.' dengan kode '.$request->code,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah membuat tipe barang baru yaitu :');

            return redirect()->route('invent.create_type')->with([
            'success' => 'Selamat! Anda berhasil memasukkan tipe baru!',
            'status' => TRUE
        ]);
    }

    /**
     * Untuk mengupdate data barang
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update_invent(Request $request, $id) 
    {
        $request->validate([
            'picture' => 'image|max:10000',
            'name' => 'required',
            'code' => 'required',
            'qty' => 'required|min:0',
        ], [
            'name.required' => 'Nama barang harus diisi',
            'condition.required' => 'Kondisi barang harus diisi',
            'qty.required' => 'Jumlah barang harus diisi',
            'picture.image' => 'Format foto tidak didukung',
            'picture.max' => 'File terlalu besar'
        ]);

        $invent = Inventories::findOrFail($id);

        //slug
        $str = str_slug($request->name);

        //image
        if($request->picture) {
            $img = $request->file('picture');
            $imgName = $str.'.'.$img->getClientOriginalExtension();
            $path = public_path('img\inventory/');
            if(File::exists($invent->picture)) {
                File::delete($invent->picture);
            }   
            $request->file('picture')->move($path, $imgName);
            $invent->picture = 'img\inventory/'.$imgName;
        }
        $invent->room_id    = $request->room_id;
        $invent->code       = $request->code;
        $invent->name       = $request->name;
        $invent->condition  = $request->condition;
        $invent->desc       = $request->desc;
        $invent->qty        = $request->qty;
        $invent->update();

        activity()
            ->withProperties([
                'obj' => $invent->name.' dengan kode '.$invent->code,
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah mengedit barang yaitu :');

        return redirect()->route('invent.create_invent')->with([
            'success' => 'Selamat! Anda berhasil mengedit barang!',
            'status' => TRUE
        ]);
    }

    /**
     * Mengupdate data ruangan
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update_room(Request $request, $id) 
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'required'
        ], [
            'name.required' => 'Nama harus diisi',
            'desc.required' => 'Deskripsi harus diisi'
        ]);

        $room = Room::findOrFail($id);
        $room->name = $request->name;
        $room->desc = $request->desc;
        $room->update();

        activity()
            ->withProperties([
                'obj' => $room->name.' dengan kode '.$room->code,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah mengedit ruangan yaitu :');

        return redirect()->route('invent.create_room')->with([
            'success' => 'Selamat! Anda berhasil mengedit ruangan!',
            'status' => TRUE
        ]);
    }

    /**
     * Mengupdate data tipe barang
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function update_type(Request $request, $id) 
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'required'
        ], [
            'name.required' => 'Nama harus diisi',
            'desc.required' => 'Deskripsi harus diisi'
        ]);

        $type = Type::findOrFail($id);
        $type->name = $request->name;
        $type->desc = $request->desc;
        $type->update();

        activity()
            ->withProperties([
                'obj' => $type->name.' dengan kode '.$type->code,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah mengedit tipe barang yaitu :');

        return redirect()->route('invent.create_type')->with([
            'success' => 'Selamat! Anda berhasil mengedit tipe!',
            'status' => TRUE
        ]);
    }

    /**
     * Menghapus data barang
     *
     * @param [type] $id
     * @return void
     */
    public function destroy_invent($id)
    {
        $invent = Inventories::findOrFail($id);
        $borrowDetail = BorrowDetail::where('inventory_id', $id)->first();
        
        if(@$borrowDetail) {
            return response()->json([
                'success' => FALSE,
                'message' => 'Barang sedang digunakan! Anda tidak bisa menghapusnya!'
            ]);
        } else {
            DB::transaction(function () use ($invent) {
                File::delete($invent->picture);
                $invent->delete();
            });

            
            activity()
            ->withProperties([
                'obj' => $invent->name.' dengan kode '.$invent->code,
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah menghapus barang yaitu :');

            return response()->json([
                'success' => TRUE,
                'message' => 'Anda berhasil menghapus data barang!'
            ]);
        }
    }

    /**
     * Menghapus data ruangan
     *
     * @param [type] $id
     * @return void
     */
    public function destroy_room($id)
    {
        $room = Room::find($id);

        activity()
            ->withProperties([
                'obj' => $room->name.' dengan kode '.$room->code,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah menghapus ruangan yaitu :');

        DB::select('call delete_room(?)', [$id]);

        return response()->json([
            'success' => TRUE,
            'message' => 'Anda berhasil menghapus data ruangan!'
        ]);
    }

    /**
     * Menghapus data tipe barang
     *
     * @param [type] $id
     * @return void
     */
    public function destroy_type($id)
    {
        $type = Type::findOrFail($id);

        activity()
            ->withProperties([
                'obj' => $type->name.' dengan kode '.$type->code,
                'by' => '<b>'.Auth::user()->admin->name.'</b>'
            ])
            ->log('telah menghapus tipe barang yaitu :');

        DB::select('call delete_type(?)', [$id]);

        return response()->json([
            'success' => TRUE,
            'message' => 'Anda berhasil menghapus data tipe!'
        ]);
    }

    /**
     * Mengambil kode barang berdasarkan {type_id}
     *
     * @param [type] $code
     * @return void
     */
    public function api_type($code)
    {
        $type = Type::findOrFail($code);
        $invent = Inventories::where('type_id', $type->id)->orderBy('code', 'desc')->first();

        if(!$invent) {
            $code = $type->code.'-0001';
        } else {
            $code = ++$invent->code;
        }

        echo json_encode(array('code' => $code));
    }

    /**
     * Mengolah data untuk dprint menjadi label
     *
     * @param [type] $codes
     * @return void
     */
    public function label($codes)
    {
        $barcode = new BarcodeGenerator();
        $barcode->setText($codes);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(2);
        $barcode->setThickness(25);
        $barcode->setFontSize(10);
        $code = $barcode->generate();

        $invent = Inventories::where('code', $codes)->first();

        activity()
            ->withProperties([
                'obj' => $invent->name.' dengan kode '.$invent->code,
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('telah mencetak label barang yaitu :');

        return view('inventory/label', compact('code', 'invent'));
    }

    /**
     * Untuk men-generate laporan data peminjaman
     *
     * @return void
     */
    public function report()
    {
        $borrow = DB::table('detail_peminjaman')->get();

        return view('admin/report', compact('borrow'));
    }
}
