<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Borrow;
use App\Inventory;
use App\User;

class ApiController extends Controller
{
    /**
     * Untuk menampilkan detail pengguna dalam bentuk modal
     *
     * @param [type] $id
     * @return void
     */
    public function user($id)
    {
        // Mendapatkan user berdasasrkan ID
        $user = User::findOrFail($id);
        $user->status = $user->approved == 0 ? "Tidak Aktif" : "Aktif"; 

        // Mengolah data untuk ditampilkan kedalam modal
        if($user->level_id == 1) {
            $user->level = 'Peminjam';
            $user->name = $user->borrower['name'];
            $user->nip = $user->borrower['nip'] ?? '-';
            $user->phone = $user->borrower['phone'];
            $user->address = $user->borrower['address'];
        } elseif ($user->level_id == 2) {
            $user->level = 'Operator';
            $user->name = $user->operator['name'];
            $user->nip = $user->operator['nip'] ?? '-';
            $user->phone = '-';
            $user->address = '-';
        } elseif ($user->level_id == 3) {
            $user->level = 'Admin';
            $user->name = $user->admin['name'];
            $user->nip = $user->admin['nip'] ?? '-';
            $user->phone = '-';
            $user->address = '-';
        }

        return view('user/modal/_detail', compact('user'));
    }

    /**
     * Menampilkan detail peminjaman dalam bentuk modal
     *
     * @param [type] $id
     * @return void
     */
    public function borrow($id)
    {
        // Mengambil data peminjaman berdasarkan ID
        $borrow = DB::table('detail_peminjaman')->where('id', $id)->get();
        return view('borrow/modal/_borrow', compact('borrow')); 
    }

    /**
     * Menampilkan detail barang dalam bentuk modal
     *
     * @param [type] $id
     * @return void
     */     
    public function invent($id)
    {
        // Mengambil data barang berasarkan ID
        $invent = Inventory::findOrFail($id);
        return view('inventory/modal/_invent', compact('invent'));
    }
}
