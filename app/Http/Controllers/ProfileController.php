<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use File;
use Validator;

use App\Borrower;
use App\Operator;
use App\Admin;
use App\User;
use App\Http\Requests\Profile;


class ProfileController extends Controller
{
    /**
     * Menampilkan halaman profile berdasarkan Role
     *
     * @return void
     */
    public function profile(){
        if(session('role') == 1){
            $profile = Borrower::where('user_id',session('id'))->limit(1)->get();
            $role = 'borrower';
        }elseif(session('role') == 2){
            $profile = Operator::where('user_id',session('id'))->limit(1)->get();
            $role = 'operator';
        }elseif(session('role') == 3){
            $profile = Admin::where('user_id', session('id'))->limit(1)->get();
            $role = 'admin';
        }
        return view('profile', ['profile' => $profile[0],'role' =>$role]);
    }

    /**
     * Mengupdate profile
     *
     * @param Request $request
     * @return void
     */
    public function update(Request $request){
        $rules = array(
            'name' => 'required',
            'picture' => 'nullable|image|max:10000',
        );

        if($request->password){
            $rules['password'] = 'min:6';
        }

        if($request->nip){
            $rules['nip'] = 'min:18';
        }

        if(session('role') == 1){
            $rules['phone'] = 'required|min:11|max:13';
            $rules['address'] = 'required';
        }
        
        $request->validate($rules,[
            'name.required' => 'Nama harus diisi',
            'password.min' => 'Password minimal memiliki :min karakter',
            'picture.image' => 'Format file tidak didukung',
            'picture.max' => 'File terlalu besar',
            'phone.required' => 'No telp harus diisi',
            'phone.min' => 'No telp minimal memiliki :min karakter',
            'phone.max' => 'No telp maksimal memiliki :max karakter',
            'address.required' => 'Alamat harus diisi',
            'nip.min' => 'NIP minimal memiliki :min karakter'
        ]);

        $user = User::find(session('id'));
        $str = str_slug($request->name);
        if($request->password){
            $user = User::where('id', session('id'))->update([
                'password' => bcrypt($request->password)
            ]);
        }

        if($request->picture){
            $picture = $request->file('picture');
            $pictureName = $str.'.'.$picture->getClientOriginalExtension();
            $path = public_path('img\profile/');

            if(File::exists($user->picture)){
                File::delete($user->picture);
            }

            $request->file('picture')->move($path,$pictureName);
            $user->picture = 'img\profile/'.$pictureName;

            $user = User::where('id', session('id'))->update([
                'picture' => 'img/profile/'.$pictureName
            ]);
        }

        if (session('role') == 1){
            Borrower::where('user_id',session('id'))->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'address' => $request->address,
                'nip' => $request->nip
            ]);
            Session::put('name',$request->name);
        }elseif (session('role') == 2){
            Operator::where('user_id', session('id'))->update([
                'name' => $request->name,
                'nip' => $request->nip
            ]);
            Session::put('name',$request->name);
        }elseif (session('role') == 3) {
            Admin::where('user_id', session('id'))->update([
                'name' => $request->name
            ]);
            Session::put('name',$request->name);
        }

        activity()
            ->withProperties([
                'obj' => date('H:i'),
                'by' => '<b>'.session('name').'</b>'
            ])
            ->log('baru saja mengupdate profile pada ');

        return redirect()->route(session('as').'.profile')->with([
            'msg' => 'Profile berhasil di update',
            'status' => true
        ]);
    }
}