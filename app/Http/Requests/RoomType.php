<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class RoomType extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|unique:rooms',
            'name' => 'required',
            'desc' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Kode  harus diisi',
            'code.unique' => 'Kode sudah digunakan',
            'name.required' => 'Nama  harus diisi',
            'desc.required' => 'Deskripsi harus diisi'
        ];
    }

}
