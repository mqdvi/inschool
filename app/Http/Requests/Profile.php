<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Profile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'password' => 'min:6',
            'phone' => 'required|min:11|max:13',
            'address' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama harus diisi',
            'password.min' => 'Password minimal memiliki :min karakter',
            'phone.required' => 'No telp harus diisi',
            'phone.min' => 'No telp minimal memiliki :min karakter',
            'phone.max' => 'No tgelp maksimal memiliki :max karakter',
            'address.required' => 'Alamat harus diisi'
        ];
    }
}
