<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class Borrow extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'condition' => 'required',
            'picture' => 'required|image|max:10000',
            'qty' => 'required|numeric',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Nama  harus diisi',
            'condition.required' => 'Kondisi  harus diisi',
            'picture.required' => 'Foto barang harus diisi',
            'picture.image' => 'Format file tidak didukung',
            'picture.max' => 'File terlalu besar',
            'qty.required' => 'Jumlah barang harus diisi',
            'qty.numeric' => 'Jumlah barang harus diisi dengan angka',
        ];
    }
}
