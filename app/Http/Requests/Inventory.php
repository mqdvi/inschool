<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class Inventory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => 'required|image|max:10000',
            'name' => 'required',
            'code' => 'required|unique:inventories',
            'qty' => 'required|gt:0',
        ];
    }

    public function messages()
    {
        return [
            'code.required' => 'Kode barang harus diisi',
            'code.unique' => 'Kode barang telah digunakan',
            'name.required' => 'Nama barang harus diisi',
            'condition.required' => 'Kondisi barang harus diisi',   
            'qty.required' => 'Jumlah barang harus diisi',
            'qty.gt' => 'Jumlah harus lebih dari 0',
            'picture.required' => 'Foto barang harus diisi',
            'picture.image' => 'Format file tidak didukung',
            'picture.max' => 'File terlalu besar'
        ];
    }
}
