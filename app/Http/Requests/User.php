<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class User extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'username' => 'required|unique:users|min:6',
            'password' => 'required|min:6',
            'phone' => 'required|min:11|max:13',
            'address' => 'required',
            'nip' => 'nullable|min:18'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama harus diisi',
            'username.required' => 'Username harus diisi',
            'username.unique' => 'Username sudah digunakan',
            'username.min' => 'Username minimal memiliki :min karakter',
            'password.required' => 'Password harus diisi',
            'password.min' => 'Password minimal memiliki :min karakter',
            'phone.required' => 'No telp harus diisi',
            'phone.min' => 'No telp minimal memiliki :min karakter',
            'phone.max' => 'No telp maksimal memiliki :max karakter',
            'address.required' => 'Alamat harus diisi',
            'nip.min' => 'NIP memiliki minimal :min karekter'
        ];
    }

}
