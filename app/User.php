<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Admin;
use App\Operator;
use App\Borrower;
use App\Level;
use App\Inventory;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level_id', 'username', 'password', 'picture', 'approved'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Untuk merelasikan dengan model admin
     *
     * @return void
     */
    public function admin ()
    {
        return $this->hasOne(Admin::class);
    }

    /**
     * Untuk merelasikan dengan model operator
     *
     * @return void
     */
    public function operator ()
    {
        return $this->hasOne(Operator::class);
    }

    /**
     * Untuk merelasikan dengan model borrower
     *
     * @return void
     */
    public function borrower ()
    {
        return $this->hasOne(Borrower::class);
    }

    /**
     * Untuk merelasikan dengan model level
     *
     * @return void
     */
    public function level ()
    {
        return $this->belongsTo(Level::class);
    }

    /**
     * Untuk merelasikan dengan model inventory
     *
     * @return void
     */
    public function inventory ()
    {
        return $this->hasMany(Inventory::class);
    }
}
