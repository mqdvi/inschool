<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Inventory;

class Type extends Model
{
    protected $fillable = ['code', 'name', 'desc'];

    /**
     * untuk merelasikan dengan model inventory
     *
     * @return void
     */
    public function inventory ()
    {
        return $this->hasMany(Inventory::class);
    }
}
