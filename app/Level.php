<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Level extends Model
{
    protected $fillable = ['role'];

    /**
     * Merelasikan dengan model user
     *
     * @return void
     */
    public function user ()
    {
        return $this->hasMany(User::class);
    }
}
