var searchData=
[
  ['borrow',['Borrow',['../class_app_1_1_borrow.html',1,'Borrow'],['../class_app_1_1_borrow_detail.html#a2672ee9fb177c0a8190c962adb119c25',1,'App\BorrowDetail\borrow()'],['../class_app_1_1_borrower.html#a2672ee9fb177c0a8190c962adb119c25',1,'App\Borrower\borrow()'],['../class_app_1_1_inventory.html#a2672ee9fb177c0a8190c962adb119c25',1,'App\Inventory\borrow()']]],
  ['borrow_2ephp',['Borrow.php',['../_borrow_8php.html',1,'']]],
  ['borrowdetail',['BorrowDetail',['../class_app_1_1_borrow_detail.html',1,'BorrowDetail'],['../class_app_1_1_borrow.html#aba25f76a07219d0ae1418a23361bf56f',1,'App\Borrow\borrowDetail()'],['../class_app_1_1_inventory.html#aba25f76a07219d0ae1418a23361bf56f',1,'App\Inventory\borrowDetail()']]],
  ['borrowdetail_2ephp',['BorrowDetail.php',['../_borrow_detail_8php.html',1,'']]],
  ['borrower',['Borrower',['../class_app_1_1_borrower.html',1,'Borrower'],['../class_app_1_1_borrow.html#ae391427701d8f54626e978a61329efb9',1,'App\Borrow\borrower()'],['../class_app_1_1_user.html#ae391427701d8f54626e978a61329efb9',1,'App\User\borrower()']]],
  ['borrower_2ephp',['Borrower.php',['../_borrower_8php.html',1,'']]]
];
