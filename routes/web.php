<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

setlocale(LC_ALL, 'IND');

Route::get('/', function () { 
    return redirect()->route('auth.login');
});

//Data Chart
Route::get('/u', 'AdminController@chart')->name('chart');

// Route Auth Section
Route::prefix('auth')->name('auth.')->group(function () {
    Route::get('login', 'AuthController@login')->name('login')->middleware('guest');
    Route::get('register', 'AuthController@register')->name('register')->middleware('guest');

    Route::post('submit', 'AuthController@submit')->name('submit');
    Route::post('save', 'AuthController@save')->name('save');
    Route::post('logout', 'AuthController@logout')->name('logout');
});

// Modal API
Route::prefix('modal')->middleware('auth')->name('modal.')->group(function () {
    Route::post('user/{id}', 'ApiController@user')->name('user');
    Route::post('invent/{id}', 'ApiController@invent')->name('invent');
    Route::post('borrow/{id}', 'ApiController@borrow')->name('borrow');
});

// Route Inventarisir
Route::prefix('inventaris')->name('invent.')->middleware('auth')->group(function () {
    Route::get('inventarisir', 'InventoryController@index')->name('index');
    Route::get('ruangan', 'InventoryController@room')->name('room');
    Route::get('tipe', 'InventoryController@type')->name('type');

    Route::get('tambah-barang', 'InventoryController@create_invent')->name('create_invent');
    Route::get('tambah-ruangan', 'InventoryController@create_room')->name('create_room')->middleware('admin');
    Route::get('tambah-tipe', 'InventoryController@create_type')->name('create_type')->middleware('admin');

    Route::get('edit-barang/{slug}', 'InventoryController@edit_invent')->name('edit_invent');
    Route::get('edit-ruangan/{id}', 'InventoryController@edit_room')->name('edit_room')->middleware('admin');
    Route::get('edit-tipe/{id}', 'InventoryController@edit_type')->name('edit_type')->middleware('admin');

    Route::post('list-barang', 'InventoryController@list_invent')->name('list_invent');
    Route::post('list-ruangan', 'InventoryController@list_room')->name('list_room');
    Route::post('list-tipe', 'InventoryController@list_type')->name('list_type');
    
    Route::post('simpan-barang', 'InventoryController@store_invent')->name('store_invent');
    Route::post('simpan-ruangan', 'InventoryController@store_room')->name('store_room')->middleware('admin');
    Route::post('simpan-tipe', 'InventoryController@store_type')->name('store_type')->middleware('admin');

    Route::post('update-barang/{id}', 'InventoryController@update_invent')->name('update_invent');
    Route::post('update-ruangan/{id}', 'InventoryController@update_room')->name('update_room')->middleware('admin');
    Route::post('update-tipe/{id}', 'InventoryController@update_type')->name('update_type')->middleware('admin');

    Route::post('delete-barang/{id}', 'InventoryController@destroy_invent')->name('destroy_invent');
    Route::post('delete-ruangan/{id}', 'InventoryController@destroy_room')->name('destroy_room')->middleware('admin');
    Route::post('delete-tipe/{id}', 'InventoryController@destroy_type')->name('destroy_type')->middleware('admin');

    Route::post('api/{room}', 'InventoryController@api_type')->name('api.type');

    Route::get('label/{code}', 'InventoryController@label')->name('label');
    Route::get('report', 'InventoryController@report')->name('report')->middleware('admin');
});

// Route Peminjaman
Route::prefix('peminjaman')->name('borrow.')->middleware('auth')->group(function () {
    Route::get('/daftar', 'BorrowController@index')->name('index');
    Route::get('/permintaan', 'BorrowController@request')->name('request');
    Route::get('/tambah', 'BorrowController@create')->name('create');
    Route::get('/edit/{id}', 'BorrowController@edit')->name('edit');
    
    Route::post('api/{code}', 'BorrowController@api')->name('api');
    Route::post('list', 'BorrowController@list')->name('list');
    Route::post('list_request', 'BorrowController@list_request')->name('list_request');
    Route::post('store', 'BorrowController@store')->name('store');
    Route::post('update/{id}', 'BorrowController@update')->name('update');
    Route::get('update-status/{id}/{param}', 'BorrowController@update_request')->name('update_request');
});

// Route Admin
Route::prefix('admin')->name('admin.')->middleware('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::get('backup', 'AdminController@backup')->name('backup');

    Route::get('/profile','ProfileController@profile')->name('profile');
    Route::post('/update', 'ProfileController@update')->name('update');

    Route::prefix('pengguna')->name('user.')->group(function () {
        Route::get('/daftar', 'UserController@index')->name('index');
        Route::get('/permintaan', 'UserController@request')->name('request');
        Route::get('/tambah', 'UserController@create')->name('create');
        Route::get('/edit/{id}', 'UserController@edit')->name('edit');

        Route::post('list', 'UserController@list')->name('list');
        Route::post('list_request', 'UserController@list_request')->name('list_request');
        Route::post('store', 'UserController@store')->name('store');
        Route::post('update/{id}', 'UserController@update')->name('update');
        Route::get('update_request/{id}', 'UserController@update_request')->name('update_request');
        Route::post('destroy/{id}', 'UserController@destroy')->name('destroy');
    });

    Route::get('/log', function () {
        $act = Activity::all();

        return view('admin/activity', compact('act'));
    })->name('log');
});

// Route Operator
Route::prefix('operator')->name('operator.')->middleware('operator')->group(function () {
    Route::get('/', 'OperatorController@index')->name('index');

    Route::get('/profile','ProfileController@profile')->name('profile');
    Route::post('/update', 'ProfileController@update')->name('update');

    Route::get('/log', function () {
        $act = Activity::all();

        return view('admin/activity', compact('act'));
    })->name('log');
});

// Route Peminjaman
// Route::prefix('peminjam')->name('peminjam.')->middleware('borrower')->group(function() {
Route::prefix('peminjam')->name('peminjam.')->group(function() {
    Route::get('/', function() {
        return view('peminjam/index');
    })->name('index');

    Route::get('/profile', function() {
        return view('profile');
    })->name('profile');

    Route::get('/keranjang', function() {
        return view('peminjam/keranjang');
    })->name('keranjang');

    Route::prefix('peminjaman')->name('borrow.')->group(function () {
        Route::get('daftar', function () {
            return view('peminjam/list-peminjaman');
        })->name('list');

        Route::get('detail/{id}', function () {
            return view('peminjam/detail-peminjaman');
        })->name('detail');
    });

    Route::prefix('inventaris')->name('invent.')->group(function () {
        Route::get('/', function () {
            return view('peminjam/inventaris');
        })->name('list');

        Route::get('detail/{id}', function () {
            return view('peminjam/detail-barang');
        })->name('detail');
    });
});