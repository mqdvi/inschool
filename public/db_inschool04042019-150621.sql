-- MySQL dump 10.16  Distrib 10.1.35-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: db_inschool
-- ------------------------------------------------------
-- Server version	10.1.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_log`
--

DROP TABLE IF EXISTS `activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` int(11) DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_log_log_name_index` (`log_name`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_log`
--

LOCK TABLES `activity_log` WRITE;
/*!40000 ALTER TABLE `activity_log` DISABLE KEYS */;
INSERT INTO `activity_log` VALUES (1,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 197.168.1.12\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 06:51:45','2019-04-01 06:51:45'),(2,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"ASDSA dengan kode ASD\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:06:34','2019-04-01 07:06:34'),(3,'log_','telah mengedit ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asdsad dengan kode \",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:06:42','2019-04-01 07:06:42'),(4,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sa dengan kode asd\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:08:32','2019-04-01 07:08:32'),(5,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"ads dengan kode asd\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:11:07','2019-04-01 07:11:07'),(6,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"ads dengan kode asd\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:11:36','2019-04-01 07:11:36'),(7,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sad dengan kode asd\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:12:09','2019-04-01 07:12:09'),(8,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sad dengan kode ads\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:13:46','2019-04-01 07:13:46'),(9,'log_','telah menghapus ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sad dengan kode ads\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 07:13:53','2019-04-01 07:13:53'),(10,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng dengan kode B1-0002\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 09:25:48','2019-04-01 09:25:48'),(11,'log_','telah menghapus ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng dengan kode B1-0002\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 09:52:36','2019-04-01 09:52:36'),(12,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asf dengan kode ASF\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 09:55:53','2019-04-01 09:55:53'),(13,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sa dengan kode sda\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 09:56:10','2019-04-01 09:56:10'),(14,'log_','telah menghapus ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asf dengan kode ASF\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 09:56:17','2019-04-01 09:56:17'),(15,'log_','telah menghapus ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sa dengan kode sda\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-01 09:56:22','2019-04-01 09:56:22'),(16,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 197.168.1.12\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 01:22:56','2019-04-02 01:22:56'),(17,'log_','telah mengedit barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng Merah dengan kode B2-0002\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 01:27:31','2019-04-02 01:27:31'),(18,'log_','telah menghapus barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asdsad dengan kode dassad\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 01:42:35','2019-04-02 01:42:35'),(19,'log_','telah menghapus barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng Merah dengan kode B2-0002\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 01:42:57','2019-04-02 01:42:57'),(20,'log_','telah mengedit barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo dengan kode A1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 02:47:28','2019-04-02 02:47:28'),(21,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo dengan kode A1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 02:48:20','2019-04-02 02:48:20'),(22,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo dengan kode A1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 02:48:40','2019-04-02 02:48:40'),(23,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo dengan kode A1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 02:49:37','2019-04-02 02:49:37'),(24,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo dengan kode A1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 02:49:46','2019-04-02 02:49:46'),(25,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo dengan kode A1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 02:50:39','2019-04-02 02:50:39'),(26,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 192.168.43.87\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 03:03:04','2019-04-02 03:03:04'),(27,'log_','telah mengedit barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 03:51:11','2019-04-02 03:51:11'),(28,'log_','telah membuat peminjaman baru yaitu : ',NULL,NULL,5,'App\\User','{\"obj\":\" pada 11:28\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 04:28:47','2019-04-02 04:28:47'),(29,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 197.168.1.12\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 05:11:50','2019-04-02 05:11:50'),(30,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"10 pada 13:03\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:03:55','2019-04-02 06:03:55'),(31,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"10 pada 13:05\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:05:24','2019-04-02 06:05:24'),(32,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"10 pada 13:07\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:07:38','2019-04-02 06:07:38'),(33,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"10 pada 13:15\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:15:18','2019-04-02 06:15:18'),(34,'log_','telah membuat peminjaman baru pada ',NULL,NULL,5,'App\\User','{\"obj\":\"13:20\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:20:40','2019-04-02 06:20:40'),(35,'log_','telah membuat peminjaman baru pada ',NULL,NULL,5,'App\\User','{\"obj\":\"13:22\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:22:31','2019-04-02 06:22:31'),(36,'log_','telah membuat peminjaman baru pada ',NULL,NULL,5,'App\\User','{\"obj\":\"13:24\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:24:39','2019-04-02 06:24:39'),(37,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"13 pada 13:35\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:35:08','2019-04-02 06:35:08'),(38,'log_','telah membuat peminjaman baru pada ',NULL,NULL,5,'App\\User','{\"obj\":\"13:42\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:42:31','2019-04-02 06:42:31'),(39,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:44:14','2019-04-02 06:44:14'),(40,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"11 pada 13:54\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:54:38','2019-04-02 06:54:38'),(41,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"12 pada 13:54\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 06:54:54','2019-04-02 06:54:54'),(42,'log_','telah mengkonfirmasi pengguna dengan username : ',NULL,NULL,5,'App\\User','{\"obj\":\"peminjam pada 15:02\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:02:25','2019-04-02 08:02:25'),(43,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:06:10','2019-04-02 08:06:10'),(44,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:06:46','2019-04-02 08:06:46'),(45,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:07:23','2019-04-02 08:07:23'),(46,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:07:41','2019-04-02 08:07:41'),(47,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:07:57','2019-04-02 08:07:57'),(48,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:08:11','2019-04-02 08:08:11'),(49,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:08:18','2019-04-02 08:08:18'),(50,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:08:36','2019-04-02 08:08:36'),(51,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:08:43','2019-04-02 08:08:43'),(52,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:08:49','2019-04-02 08:08:49'),(53,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:08:59','2019-04-02 08:08:59'),(54,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:09:04','2019-04-02 08:09:04'),(55,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:09:08','2019-04-02 08:09:08'),(56,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:09:21','2019-04-02 08:09:21'),(57,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:09:25','2019-04-02 08:09:25'),(58,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:09:31','2019-04-02 08:09:31'),(59,'log_','telah mencetak label barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Headphone RAZIOIS dengan kode 8994705034762\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:09:41','2019-04-02 08:09:41'),(60,'log_','telah mengedit barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Sapu dengan kode B1-0001\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 08:11:37','2019-04-02 08:11:37'),(61,'log_','baru saja mengupdate profile pada ',NULL,NULL,5,'App\\User','{\"obj\":\"16:20\",\"by\":\"<b>Administrator<\\/b>\"}','2019-04-02 09:20:09','2019-04-02 09:20:09'),(62,'log_','baru saja mengupdate profile pada ',NULL,NULL,5,'App\\User','{\"obj\":\"16:20\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 09:20:22','2019-04-02 09:20:22'),(63,'log_','telah menghapus pengguna yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asfasf pada 16:32\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 09:32:03','2019-04-02 09:32:03'),(64,'log_','telah membuat Akun baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\" dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 09:53:43','2019-04-02 09:53:43'),(65,'log_','telah membuat Akun baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\" dengan username divasz\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 09:54:18','2019-04-02 09:54:18'),(66,'log_','telah membuat Akun baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\" dengan username safasfsaf\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 09:56:45','2019-04-02 09:56:45'),(67,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:15:31','2019-04-02 10:15:31'),(68,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:16:42','2019-04-02 10:16:42'),(69,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:18:14','2019-04-02 10:18:14'),(70,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:22:46','2019-04-02 10:22:46'),(71,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:23:19','2019-04-02 10:23:19'),(72,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan Ag dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:24:12','2019-04-02 10:24:12'),(73,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan Ag dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:24:49','2019-04-02 10:24:49'),(74,'log_','telah menghapus pengguna yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"tokoyaki pada 17:27\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:27:17','2019-04-02 10:27:17'),(75,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan Ag dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:27:32','2019-04-02 10:27:32'),(76,'log_','telah mengedit Akun yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Cogan Ag dengan username cogans\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:27:49','2019-04-02 10:27:49'),(77,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 197.168.1.12\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-02 10:42:34','2019-04-02 10:42:34'),(78,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 197.168.1.6\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 03:40:03','2019-04-04 03:40:03'),(79,'log_','telah melakukan login pada',NULL,NULL,5,'App\\User','{\"obj\":\"sistem dengan IP 197.168.1.6\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 06:11:47','2019-04-04 06:11:47'),(80,'log_','telah membuat Akun baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Tokoyaki dengan username deenss\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 06:27:25','2019-04-04 06:27:25'),(81,'log_','telah mengkonfirmasi pengguna dengan username : ',NULL,NULL,5,'App\\User','{\"obj\":\"deenss pada 13:28\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 06:28:40','2019-04-04 06:28:40'),(82,'log_','telah menghapus pengguna yaitu : ',NULL,NULL,5,'App\\User','{\"obj\":\"safasfsaf pada 13:43\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 06:43:49','2019-04-04 06:43:49'),(83,'log_','telah menghapus pengguna yaitu : ',NULL,NULL,5,'App\\User','{\"obj\":\"deenss pada 13:43\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 06:43:54','2019-04-04 06:43:54'),(84,'log_','telah menghapus pengguna yaitu : ',NULL,NULL,5,'App\\User','{\"obj\":\"cogans pada 13:44\",\"by\":\"<b>Administrator-san<\\/b>\"}','2019-04-04 06:44:08','2019-04-04 06:44:08'),(85,'log_','baru saja mengupdate profile pada ',NULL,NULL,5,'App\\User','{\"obj\":\"13:44\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 06:44:57','2019-04-04 06:44:57'),(86,'log_','telah membuat barang baru yaitu : ',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng Merah pada 14:07\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:07:01','2019-04-04 07:07:01'),(87,'log_','telah mengedit barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng Merahs dengan kode B1-0064\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:07:36','2019-04-04 07:07:36'),(88,'log_','telah menghapus barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Obeng Merahs dengan kode B1-0064\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:20:24','2019-04-04 07:20:24'),(89,'log_','telah menghapus barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Pulpen dengan kode B1-0003\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:29:46','2019-04-04 07:29:46'),(90,'log_','telah membuat barang baru yaitu : ',NULL,NULL,5,'App\\User','{\"obj\":\"sadsad pada 14:30\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:30:43','2019-04-04 07:30:43'),(91,'log_','telah menghapus barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sadsad dengan kode A1-00031\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:30:50','2019-04-04 07:30:50'),(92,'log_','telah mengedit barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"Laptop Thinkpad Lenovo uwu dengan kode A1-0001\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:31:02','2019-04-04 07:31:02'),(93,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"12 pada 14:57\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:57:06','2019-04-04 07:57:06'),(94,'log_','telah membuat ruangan baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asdsad dengan kode asda\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:59:37','2019-04-04 07:59:37'),(95,'log_','telah mengedit ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asdsadsda dengan kode asda\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 07:59:45','2019-04-04 07:59:45'),(96,'log_','telah menghapus ruangan yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"asdsadsda dengan kode asda\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:02:31','2019-04-04 08:02:31'),(97,'log_','telah membuat tipe barang baru yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sadasd dengan kode asd\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:02:46','2019-04-04 08:02:46'),(98,'log_','telah mengedit tipe barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sadasdasd dengan kode asd\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:02:55','2019-04-04 08:02:55'),(99,'log_','telah menghapus tipe barang yaitu :',NULL,NULL,5,'App\\User','{\"obj\":\"sadasdasd dengan kode asd\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:03:56','2019-04-04 08:03:56'),(100,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"12 pada 15:04\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:04:18','2019-04-04 08:04:18'),(101,'log_','telah mengedit peminjaman dengan ID : ',NULL,NULL,5,'App\\User','{\"obj\":\"10 pada 15:04\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:04:40','2019-04-04 08:04:40'),(102,'log_','telah mengkonfirmasi pengguna dengan username : ',NULL,NULL,5,'App\\User','{\"obj\":\"divasz pada 15:05\",\"by\":\"<b>Administrator-san!<\\/b>\"}','2019-04-04 08:05:48','2019-04-04 08:05:48');
/*!40000 ALTER TABLE `activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admins_user_id_index` (`user_id`),
  CONSTRAINT `admins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,1,'Administrator',NULL,NULL),(2,5,'Administrator-san!',NULL,'2019-04-04 06:44:57');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrow_details`
--

DROP TABLE IF EXISTS `borrow_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrow_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `borrow_id` int(10) unsigned NOT NULL,
  `inventory_id` int(10) unsigned NOT NULL,
  `qty` int(10) unsigned NOT NULL,
  `status` enum('Pending','Ditolak','Siap','Dipinjam','Belum Dikembalikan','Sudah Dikembalikan') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borrow_details_borrow_id_inventory_id_index` (`borrow_id`,`inventory_id`),
  KEY `borrow_details_inventory_id_foreign` (`inventory_id`),
  CONSTRAINT `borrow_details_borrow_id_foreign` FOREIGN KEY (`borrow_id`) REFERENCES `borrows` (`id`),
  CONSTRAINT `borrow_details_inventory_id_foreign` FOREIGN KEY (`inventory_id`) REFERENCES `inventories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrow_details`
--

LOCK TABLES `borrow_details` WRITE;
/*!40000 ALTER TABLE `borrow_details` DISABLE KEYS */;
INSERT INTO `borrow_details` VALUES (1,1,1,1,'Dipinjam',NULL,NULL),(2,2,2,1,'Dipinjam',NULL,NULL),(3,10,5,1,'Pending','2019-04-02 04:28:47','2019-04-04 08:04:40'),(4,11,5,2,'Dipinjam','2019-04-02 06:20:40','2019-04-02 06:54:38'),(5,12,5,5,'Ditolak','2019-04-02 06:22:31','2019-04-04 08:04:18'),(6,13,5,2,'Dipinjam','2019-04-02 06:24:39','2019-04-02 06:35:08'),(7,14,6,1,'Dipinjam','2019-04-02 06:42:31','2019-04-02 06:42:31');
/*!40000 ALTER TABLE `borrow_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`inschool`@`localhost`*/ /*!50003 TRIGGER `decrease_goods_qty` AFTER INSERT ON `borrow_details` FOR EACH ROW BEGIN
 UPDATE inventories SET qty = qty - NEW.qty
 WHERE id = NEW.inventory_id AND NEW.status = "Dipinjam";
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`inschool`@`localhost`*/ /*!50003 TRIGGER `increase_goods_qty` AFTER UPDATE ON `borrow_details` FOR EACH ROW BEGIN
 IF NEW.status="Sudah Dikembalikan" THEN
 UPDATE inventories set qty = qty + NEW.qty
 WHERE id = NEW.inventory_id AND NEW.status = "Sudah Dikembalikan";
 END IF;
 IF NEW.status="Dipinjam" THEN
 UPDATE inventories set qty = qty - NEW.qty
 WHERE id = NEW.inventory_id;
 END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `borrowers`
--

DROP TABLE IF EXISTS `borrowers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrowers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borrowers_user_id_index` (`user_id`),
  CONSTRAINT `borrowers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrowers`
--

LOCK TABLES `borrowers` WRITE;
/*!40000 ALTER TABLE `borrowers` DISABLE KEYS */;
INSERT INTO `borrowers` VALUES (1,3,'161710331161710331','M. Qadafi','081393300816','Jl. Kalong',NULL,NULL),(2,4,'161710332161710332','Lindel','081393300815','Jl. Kalong',NULL,NULL),(3,6,NULL,'Peminjam','08100000000','SMKN 1 Kota Bekasi','2019-04-02 03:02:51','2019-04-02 03:02:51'),(4,10,NULL,'Divas','081393300816',NULL,'2019-04-02 09:54:18','2019-04-02 09:54:18');
/*!40000 ALTER TABLE `borrowers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrows`
--

DROP TABLE IF EXISTS `borrows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `borrower_id` int(10) unsigned NOT NULL,
  `borrow_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `return_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `borrows_borrower_id_index` (`borrower_id`),
  CONSTRAINT `borrows_borrower_id_foreign` FOREIGN KEY (`borrower_id`) REFERENCES `borrowers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrows`
--

LOCK TABLES `borrows` WRITE;
/*!40000 ALTER TABLE `borrows` DISABLE KEYS */;
INSERT INTO `borrows` VALUES (1,1,'2019-04-01 02:41:42',NULL,NULL,NULL),(2,2,'2019-04-01 02:41:42',NULL,NULL,NULL),(10,1,'2019-02-03 17:00:00','2019-11-03 17:00:00','2019-04-02 04:28:47','2019-04-04 08:04:40'),(11,2,'2019-04-01 17:00:00',NULL,'2019-04-02 06:20:40','2019-04-02 06:20:40'),(12,3,'2019-02-03 17:00:00','2019-04-03 17:00:00','2019-04-02 06:22:31','2019-04-04 07:57:06'),(13,1,'2019-02-03 17:00:00','2019-04-01 17:00:00','2019-04-02 06:24:39','2019-04-02 06:35:08'),(14,1,'2019-04-01 17:00:00',NULL,'2019-04-02 06:42:31','2019-04-02 06:42:31');
/*!40000 ALTER TABLE `borrows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `detail_peminjaman`
--

DROP TABLE IF EXISTS `detail_peminjaman`;
/*!50001 DROP VIEW IF EXISTS `detail_peminjaman`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `detail_peminjaman` (
  `id` tinyint NOT NULL,
  `borrower_id` tinyint NOT NULL,
  `borrow_at` tinyint NOT NULL,
  `inventory_id` tinyint NOT NULL,
  `qty` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `nama_barang` tinyint NOT NULL,
  `picture` tinyint NOT NULL,
  `return_at` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `inventories`
--

DROP TABLE IF EXISTS `inventories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condition` enum('Baik','Rusak') COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `qty` int(10) unsigned NOT NULL DEFAULT '0',
  `picture` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `inventories_user_id_type_id_room_id_index` (`user_id`,`type_id`,`room_id`),
  KEY `inventories_type_id_foreign` (`type_id`),
  KEY `inventories_room_id_foreign` (`room_id`),
  CONSTRAINT `inventories_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `inventories_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `inventories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventories`
--

LOCK TABLES `inventories` WRITE;
/*!40000 ALTER TABLE `inventories` DISABLE KEYS */;
INSERT INTO `inventories` VALUES (1,2,1,2,'A1-0001','Laptop Thinkpad Lenovo uwu','Baik',NULL,9,'img\\inventory/laptop-thinkpad-lenovo.jpg','laptop-thinkpad-lenovo-uwu','2019-03-31 21:00:00','2019-04-04 07:31:02'),(2,2,2,1,'B1-0001','Sapu','Baik',NULL,9,'img\\inventory/sapu.jpg','sapu','2019-03-31 20:00:00','2019-04-02 08:11:37'),(5,5,1,1,'8994705034762','Headphone RAZIOIS','Baik',NULL,6,'img/inventory/headphone-raziois.jpg','headphone-raziois','2019-04-01 09:40:53','2019-04-02 03:51:11'),(6,5,1,1,'A1-0003','Laptop','Baik','Laptop thongpad',4,'img/inventory/laptop.jpg','laptop','2019-04-01 09:53:03','2019-04-01 09:53:03');
/*!40000 ALTER TABLE `inventories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `levels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels`
--

LOCK TABLES `levels` WRITE;
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
INSERT INTO `levels` VALUES (1,'Peminjam',NULL,NULL),(2,'Operator',NULL,NULL),(3,'Admin',NULL,NULL);
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_02_22_061618_create_activity_log_table',1),(4,'2019_03_10_143149_create_types_table',1),(5,'2019_03_10_143206_create_rooms_table',1),(6,'2019_03_10_143213_create_levels_table',1),(7,'2019_03_10_143224_create_admins_table',1),(8,'2019_03_10_143235_create_operators_table',1),(9,'2019_03_10_143253_create_borrowers_table',1),(10,'2019_03_10_143306_create_borrows_table',1),(11,'2019_03_10_143315_create_inventories_table',1),(12,'2019_03_10_143332_create_borrow_details_table',1),(13,'2019_03_10_143406_add_foreign_level',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `new_users`
--

DROP TABLE IF EXISTS `new_users`;
/*!50001 DROP VIEW IF EXISTS `new_users`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `new_users` (
  `picture` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `phone` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `operators`
--

DROP TABLE IF EXISTS `operators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operators_user_id_index` (`user_id`),
  CONSTRAINT `operators_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operators`
--

LOCK TABLES `operators` WRITE;
/*!40000 ALTER TABLE `operators` DISABLE KEYS */;
INSERT INTO `operators` VALUES (1,2,'Operator','161710331161710331','197.168.1.13',NULL,'2019-04-01 04:47:48');
/*!40000 ALTER TABLE `operators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'D9','Ruang 29','Gudang',NULL,NULL),(2,'D8','Ruang 28','Gudang',NULL,NULL),(3,'D10','Ruang 30','Ruang Kelas (Gedung D)','2019-04-01 06:23:15','2019-04-01 06:23:15');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `types`
--

DROP TABLE IF EXISTS `types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `types`
--

LOCK TABLES `types` WRITE;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` VALUES (1,'A1','Teknologi','Contoh : Laptop, Mouse, Keyboard, dll.',NULL,NULL),(2,'B1','Peralatan','Contoh : Sapu, Pel, Kemoceng, dll.',NULL,NULL);
/*!40000 ALTER TABLE `types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `level_id` int(10) unsigned NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_level_id_index` (`level_id`),
  CONSTRAINT `users_level_id_foreign` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,3,'admin','admin',NULL,1,NULL,NULL,NULL),(2,2,'operator','$2y$10$TX7FlAQa67TIwLrqsGZpJuZzezWHObpeYlPQ2i7RK1MD9HleRvHtG',NULL,1,'EzIY2xbffBRBx07UhLNEHnPn9eCcg4pkksUYGZpwbRtzbF6Kq2SU7zJMWIUe',NULL,NULL),(3,1,'mqdvi','mqdvi',NULL,1,NULL,NULL,NULL),(4,1,'lindel','lindel',NULL,1,NULL,NULL,NULL),(5,3,'admins','$2y$10$TX7FlAQa67TIwLrqsGZpJuZzezWHObpeYlPQ2i7RK1MD9HleRvHtG','img/profile/administrator-san.jpg',1,'gq4Cp1bysoQlRf4RxH13syXBgcJk2ikQMXPnONfFtV4gAuqXDRp2Ox7P7AbM','2019-04-01 04:31:24','2019-04-04 06:44:57'),(6,1,'peminjam','$2y$10$cItRk1zqH3JYxIfRUg8N4.b5UP5C3Mfsv2Xc9oUGORFaFmoOdCfe6',NULL,1,NULL,'2019-04-02 03:02:51','2019-04-02 08:02:25'),(10,1,'divasz','$2y$10$ysYykW0LkBtf15a.QuJnv.wizU2KzrWUvdBp7pczd947uCsPu4LUe','img/profile/divas.jpeg',1,NULL,'2019-04-02 09:54:18','2019-04-04 08:05:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `detail_peminjaman`
--

/*!50001 DROP TABLE IF EXISTS `detail_peminjaman`*/;
/*!50001 DROP VIEW IF EXISTS `detail_peminjaman`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `detail_peminjaman` AS select `borrows`.`id` AS `id`,`borrows`.`borrower_id` AS `borrower_id`,`borrows`.`borrow_at` AS `borrow_at`,`borrow_details`.`inventory_id` AS `inventory_id`,`borrow_details`.`qty` AS `qty`,`borrow_details`.`status` AS `status`,`borrowers`.`name` AS `name`,`inventories`.`name` AS `nama_barang`,`users`.`picture` AS `picture`,`borrows`.`return_at` AS `return_at` from ((((`borrows` join `borrow_details` on((`borrow_details`.`borrow_id` = `borrows`.`id`))) join `inventories` on((`inventories`.`id` = `borrow_details`.`inventory_id`))) join `borrowers` on((`borrowers`.`id` = `borrows`.`borrower_id`))) join `users` on((`users`.`id` = `borrowers`.`user_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `new_users`
--

/*!50001 DROP TABLE IF EXISTS `new_users`*/;
/*!50001 DROP VIEW IF EXISTS `new_users`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `new_users` AS select `u`.`picture` AS `picture`,`b`.`name` AS `name`,`users_status`(1) AS `status`,`b`.`phone` AS `phone` from (`users` `u` join `borrowers` `b`) where (`u`.`id` = `b`.`user_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-04 15:06:22
