@extends('auth')

@section('content')
{{-- Main content --}}
<div class="container-fluid">
    <div class="row full-height">
        <div class="col-sm-12 col-md-5">

            <div class="content full-width-xs">
                <div class="vertical-align">
                    <div class="row">
                        {{-- Brand image --}}
                        <div class="col-12">
                            <img src=" {{ asset('img/logo/inschool-text.png') }} " alt="" style="height: 15vh">
                        </div>
                        <div class="col-12">
                            <p>Masuk atau daftarkan diri anda sekarang!</p>
                        </div>
                        <div class="col-12 mt-5">
                            {{-- Login form --}}
                            <form action="{{ route('auth.submit') }}" method="POST">
                                @csrf
                                <div class="form-group {{ $errors->has('username') ? 'has-danger' : '' }}">
                                    <input type="text" name="username" id="username" class="form-control rounded-100 {{ $errors->has('username') ? 'is-invalid' : '' }}" placeholder="Username" value="{{ old('username') }}">
                                    @if($errors->has('username'))
                                        <span class="invalid-feedback text-left" role="alert">
                                            <strong>{{ $errors->first('username')}}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
                                    <input type="password" name="password" id="password" class="form-control rounded-100 {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password" value="{{ old('password') }}">
                                    @if($errors->has('password'))
                                        <span class="invalid-feedback text-left" role="alert">
                                            <strong>{{ $errors->first('password')}}</strong>
                                        </span>
                                    @endif
                                </div>
                                @if(!empty($msg))
                                    <div class="mb-3 text-danger">
                                        {{ $msg }}
                                    </div>
                                @endif
                                {{-- Login button --}}
                                <div class="form-group">
                                    <input type="submit" name="submit" id="submit" value="Masuk" class="btn btn-block bg-theme rounded-100">
                                </div>
                            </form>
                            {{-- End of login form --}}
                            <p class="h6 text-center mb-0">ATAU</p>
                            <div class="dropdown-divider"></div> 
                            {{-- Link to register page --}}
                            <p>Tidak punya akun ? <a href="{{ route('auth.register') }}"> Daftar Sekarang!</a></p>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
        {{-- Login image --}}
        <div class="col-sm-12 col-md-7 bg-auth d-none d-lg-block">
        </div>
    </div>
</div>
@endsection