<div class="modal-header">
    <h6 class="modal-title mb-0">Detail Barang</h6>
</div>

<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 pl-0">
                <img src="{{ asset('img/no-photo.png') }}" class="img-fluid">
            </div>
            <div class="col-8 pr-0">
                @foreach ($borrow as $item)
                    <div class="row">
                        <div class="col-6 mb-3">
                            <small class="text-bold">Nama Peminjam</small>
                            <div class="modal-text">{{ $item->name }}</div>
                        </div>
                        <div class="col-6 mb-3">
                            <small class="text-bold">Barang</small>
                            <div class="modal-text">{{ $item->nama_barang }}</div>
                        </div>
                        <div class="col-6 mb-3">
                            <small class="text-bold">Jumlah</small>
                            <div class="modal-text text-bold">{{ $item->qty }}</div>
                        </div>
                        <div class="col-6 mb-3">
                            <small class="text-bold">Status</small>
                            <div class="modal-text">{{ $item->status }}</div>
                        </div>
                        <div class="col-6 mb-3">
                            <small class="text-bold">Waktu Peminjaman</small>
                            <div class="modal-text">{{ date('d F Y', strtotime($item->borrow_at)) }}</div>
                        </div>
                        <div class="col-6 mb-3">
                            <small class="text-bold">Waktu Pengembalian</small>
                            <div class="modal-text">{{ $item->return_at ? date('d F Y', strtotime($item->return_at)) : "-" }}</div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="modal-footer py-3">
    <button class="btn btn-base" onclick="$('.modal').modal('hide')">Tutup</button>
</div>