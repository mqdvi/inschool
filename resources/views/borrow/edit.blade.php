@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form Edit Peminjaman</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('borrow.update', @$data->id) }}" method="post">
                                @csrf
                                <div class="row">
                                    
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Status</label>
                                                    <select name="status" class="form-control">
                                                        <option value="Pending" {{ $data->status['status'] === "Pending" ? 'selected' : '' }}>Pending</option>
                                                        <option value="Dipinjam" {{ $data->status['status'] === "Dipinjam" ? 'selected' : '' }}>Dipinjam</option>
                                                        <option value="Ditolak" {{ $data->status['status'] === "Ditolak" ? 'selected' : '' }}>Ditolak</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tanggal Pinjam</label>
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                        </div>
                                                        <input class="form-control datepicker" name="borrow_at" placeholder="Select date" type="text" value="{{ date('m/d/Y', strtotime(@$data->borrow_at)) }}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tanggal Kembali</label>
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend bg-secondary">
                                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                        </div>
                                                        <input class="form-control datepicker" name="return_at" placeholder="Select date" type="text" value="{{ @$data->return_at ? date('d/m/Y', strtotime(@$data->return_at)) : '' }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('borrow.index') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function() {
            if({{ session('status') ?? '0' }}) {
                swal({
                    title: "Selamat!",
                    text: "{{ session('success') }}",
                    icon: "success",
                    buttons: true,
                }).then((willRedirect) => {
                    if(willRedirect) {
                        window.location = '{{ route("borrow.index") }}'
                    }
                })                   
            }
        })
    </script>
@endsection