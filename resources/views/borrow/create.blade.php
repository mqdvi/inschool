@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form Tambah Peminjaman</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            <form action="{{ route('borrow.store') }}" method="post">
                                @csrf
                                <input type="text" name="invent_id" id="invent_id" hidden>
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="choose-photo bg-light">
                                            <img src="" id="picture" class="img-fluid">
                                        </div>
                                    </div>

                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Kode barang</label>
                                                    <input type="text" id="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" autofocus>
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('code') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nama barang</label>
                                                    <input type="text" id="stuff" class="form-control" disabled>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tipe barang</label>
                                                    <input type="text" id="type" class="form-control" disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Ruangan</label>
                                                    <input type="text" id="room" class="form-control" disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Kondisi barang</label>
                                                    <input type="text" id="condition" class="form-control" disabled>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Jumlah</label>
                                                    <input type="text" name="qty" class="form-control {{ $errors->has('qty') ? 'is-invalid' : '' }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('qty') }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Peminjam</label>
                                                    <select name="borrower_id" class="form-control">

                                                        @foreach ($borrower as $item)
                                                            <option value="{{ $item->id }}" {{ $item->id == @$data->borrower_id ? 'selected' : '' }}>{{ $item->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tanggal Pinjam</label>
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                        </div>
                                                        <input class="form-control datepicker" name="borrow_at" placeholder="Select date" type="text" value="{{ @$data->borrow_at ?? date('m/d/Y') }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tanggal Kembali</label>
                                                    <div class="input-group input-group-alternative">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                        </div>
                                                        <input class="form-control datepicker" name="return_at" placeholder="Select date" type="text" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Status</label>
                                                    <select name="status" class="form-control">
                                                        <option value="Pending">Pending</option>
                                                        <option value="Dipinjam">Dipinjam</option>
                                                        <option value="Ditolak">Ditolak</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('borrow.index') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function() {
            $(":input").keypress(function(event){
                if (event.which == '10' || event.which == '13') {
                    event.preventDefault();
                }
            });

            $('#code').on('change', function(e) {
                e.preventDefault();
                var code = $(this).val();
                console.log(code)

                axios.post('{{ url("peminjaman/api") }}/' + code)
                .then((res) => {
                    var result = res.data;
                    var path = '{{ asset("/") }}';
                    console.log(result)
                    console.log(path)

                    $('#stuff').val(result.name)
                    $('#type').val(result.types)
                    $('#room').val(result.rooms)
                    $('#condition').val(result.condition)
                    $('#invent_id').val(result.id)
                    $('#picture').attr('src', path+result.picture)
                })
            })
        })
        
    </script>
@endsection