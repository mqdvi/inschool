@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form {{ @$data ? "Edit" : "Tambah" }} Barang</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            @if (@$data)
                                <form action="{{ route('invent.update_invent', @$data->id) }}" method="post" enctype="multipart/form-data">
                            @else
                                <form action="{{ route('invent.store_invent') }}" method="post" enctype="multipart/form-data">
                            @endif
                                @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="choose-photo bg-light {{ $errors->has('picture') ? 'has-error' : '' }}">
                                            <img src="{{ @$data->picture ? asset(@$data->picture) : '' }}" class="img-fluid is-invalid" id="img-file">
                                        </div>
                                        <span class="text-danger">
                                            {{ $errors->has('picture') ? 'Format file tidak didukung' : '' }}
                                        </span>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="file" name="picture" class="img-preview d-none">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nama barang</label>
                                                    <input type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name', @$data->name) }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('name') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Tipe barang</label>
                                                    <select name="type_id" id="type" class="form-control" {{ @$data ? 'disabled' : '' }}>

                                                        @foreach ($type as $item)
                                                            <option value="{{ $item->id }}" {{ @$data->type_id === $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Kode barang</label>
                                                    <input class="form-control disabled {{ $errors->has('code') ? 'is-invalid' : '' }}" type="text" name="code" value="{{ old('code', @$data->code) }}" readonly>
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('code') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Ruangan</label>
                                                    <select name="room_id" id="room" class="form-control">
                                                        
                                                        @foreach ($room as $rooms)
                                                            <option value="{{ $rooms->id }}" {{ @$data->room_id === $rooms->id ? 'selected' : '' }}>{{ $rooms->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Kondisi barang</label>
                                                    <select name="condition" id="" class="form-control">
                                                        <option value="Baik" {{ @$data->condition == "Baik" ? 'selected' : '' }}>Baik</option>
                                                        <option value="Rusak" {{ @$data->condition == "Rusak" ? 'selected' : '' }}>Rusak</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Stok</label>
                                                    <input type="number" name="qty" class="form-control {{ $errors->has('qty') ? 'is-invalid' : '' }}" value="{{ old('qty', @$data->qty) }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('qty') }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Deskripsi</label>
                                                    <textarea name="desc" class="form-control {{ $errors->has('desc') ? 'is-invalid' : '' }}" rows="4">{{ old('desc', @$data->desc) }}</textarea>       
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('desc') }}
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                  

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('invent.index') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function() {
            var code = $('#type').val();

            if({{ @$data ? 0 : 1 }}) {
                axios.post("{{ url('inventaris/api') }}/" + code)
                .then((res) => {
                    console.log(res.data);
                    var result = res.data;
                    $('[name="code"]').val(result.code);
                }).catch((err) => {
                    console.log(err);
                })
            }

            $('#type').change(() => {
                code = $('#type').val();
                
                axios.post("{{ url('inventaris/api') }}/" + code)
                .then((res) => {
                    console.log(res.data);
                    var result = res.data;
                    $('[name="code"]').val(result.code);
                }).catch((err) => {
                    console.log(err);
                })
            })

            if({{ session('status') ?? '0' }}) {
                swal({
                    title: "Selamat!",
                    text: "{{ session('success') }}",
                    icon: "success",
                    buttons: true,
                }).then((willRedirect) => {
                    if(willRedirect) {
                        window.location = '{{ route("invent.index") }}'
                    }
                })                   
            }
        })
    </script>
@endsection