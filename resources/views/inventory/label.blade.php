<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>INSCHOOL — {{ ucfirst(Request::segment(2) ?? "beranda") }}</title>

    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/logo/inschool.png') }}" type="image/x-icon">
</head>
<style>
    body {
        border: 1px solid black;
        width: 29mm;
        height: 89.8mm;
    }

    .img-smk {
        transform: rotate(90deg) translate(5%, -55%);
    }

    .img-label {
        margin-right: 5px;
        margin-left: 5px;
        transform: rotate(90deg) translate(37%, 125%);
    }

    .img-logo {
        transform: rotate(90deg) translate(395%, -55%);
    }
</style>
<body>
    <div style="margin: auto;">
        <img src="{{ asset('img/logo/smkn1.jpg') }}" style="width:45px;height:45px;" class="img-smk">
        <img src="data:image/png;base64,{{ $code }}" class="img-label" />
        <img src="{{ asset('img/logo/inschool.png') }}" style="width:45px;height:45px;" class="img-logo">
    </div style="margin: auto;">
    <div style="position: relative; top: 175px;">
        <small>
            <strong>Ruangan</strong>
            <br>
            {{ $invent->room->name }}
            <br>
            <strong>Nama Barang</strong>
            <br>
            {{ $invent->name }}
        </small>
    </div>
</body>
<script>
    // window.print()
</script>
</html>