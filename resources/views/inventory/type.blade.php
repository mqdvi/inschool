@extends('template')

@section('content')

    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9">
                            <form class="form-inline">
                                <div class="form-group">
                                    <div class="input-group input-group-merge">
                                        <input class="form-control" placeholder="Search" type="text" id="search">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ml-3">
                                    <select id="sortBy" class="form-control">
                                        <option>Sort By</option>
                                        <option value="asc">Code (A-Z)</option>
                                        <option value="desc">Code (Z-A)</option>
                                    </select>
                                </div>
                            </form>	
                        </div>
                        @if (Auth::user()->level_id === 3)
                            <div class="col-sm-3 text-right">
                                <a href="{{ route('invent.create_type') }}" class="btn btn-base btn-rounded">
                                    <i class="fa fa-plus"></i> Tambah Tipe Barang
                                </a>
                            </div>
                        @endif
                    </div>
                            
                    <div class="row mt-2">
                        <table id="myTable" class="table mt-3">
                            <thead class="thead-base">
                                <tr>
                                    <th class="text-center" width="150">Kode Tipe</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th class="text-center" width="150">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var table;
        $(function () {
            table = $('#myTable').dataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bSort": true,
                "bFilter": true,
                "sDom": 'ltipr',
                "processing": true,
                "serverside": true,
                "ajax": {
                    url: '{{ route("invent.list_type") }}',
                    type: "POST"
                },
                "columns": [
                    {"data": "code", "sClass": "hide-sort text-center align-middle", "bSortable": true},
                    {"data": "name", "sClass": "hide-sort align-middle", "bSortable": false},
                    {"data": "desc", "sClass": "align-middle", "bSortable": false},
                    {"data": "action", "sClass": "text-center align-middle", "bSortable": false}
                ],
                "aaSorting": [[0, "asc"]],
                "drawCallback": function (settings) {
                    $('.tip-right').tooltip({
                        container: 'body',
                        placement: 'right'
                    });
                }
            });

            $(".dataTables_length").remove();
            $(".mytable_filter").remove();

            $("#search").keyup(function(){
                table.fnFilter($(this).val());
            });

            $("#sortBy").change(function(){
                var val = $(this).val();
                table.fnSort([0, val]);
            });
        });

        function deleteData(id) {
            swal({
                title: "Data ini akan dihapus secara permanen!",
                text: "Apakah Anda yakin?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then(() => {
                axios.post('{{ url("inventaris/delete-tipe") }}/' + id)
                .then((res) => {
                    var result = res.data;
                    swal(result.message, {icon: "success"});
                    table.fnReloadAjax();
                }).catch((err) => {
                    console.log(err)
                })
            });

        }
    </script>
@endsection