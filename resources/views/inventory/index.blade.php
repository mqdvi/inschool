@extends('template')

@section('content')

    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9">
                            <form class="form-inline">
                                <div class="form-group">
                                    <div class="input-group input-group-merge">
                                        <input class="form-control" placeholder="Search" type="text" id="search">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ml-3">
                                    <select id="sortBy" class="form-control">
                                        <option>Sort By</option>
                                        <option value="asc">Name (A-Z)</option>
                                        <option value="desc">Name (Z-A)</option>
                                    </select>
                                </div>
                            </form>	
                        </div>
                        <div class="col-sm-3 text-right">
                            <a href="{{ route('invent.create_invent') }}" class="btn btn-base btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Barang
                            </a>
                        </div>
                    </div>
                            
                    <div class="row mt-2">
                        <table id="myTable" class="table mt-3">
                            <thead class="thead-base">
                                <tr>
                                    <th width="10"></th>
                                    <th>Nama</th>
                                    <th width="100">Ruangan</th>
                                    <th width="100">Kode Barang</th>
                                    <th width="80" class="text-center">Kondisi</th>
                                    <th width="80" class="text-center">Stok</th>
                                    <th width="50">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var table;
        $(function () {
            table = $('#myTable').dataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bSort": true,
                "bFilter": true,
                "sDom": 'ltipr',
                "processing": true,
                "serverside": true,
                "ajax": {
                    url: '{{ route("invent.list_invent") }}',
                    type: "POST"
                },
                "columns": [
                    {"data": "picture", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "name", "sClass": "hide-sort align-middle", "bSortable": true},
                    {"data": "room_id", "sClass": "align-middle", "bSortable": false},
                    {"data": "code", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "condition", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "qty", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "action", "sClass": "text-center align-middle", "bSortable": false}
                ],
                "aaSorting": [[1, "asc"]],
                "drawCallback": function (settings) {
                    $('.tip-right').tooltip({
                        container: 'body',
                        placement: 'right'
                    });
                }
            });

            $(".dataTables_length").remove();
            $(".mytable_filter").remove();

            $("#search").keyup(function(){
                table.fnFilter($(this).val());
            });

            $("#sortBy").change(function(){
                var val = $(this).val();
                table.fnSort([1, val]);
            });
        });

        function printLabel(id) {
            swal({
                title: "Apakah Anda ingin print labelnya ?",
                text: "Klik tombol \'ok\' untuk lanjut !",
                icon: "info",
                buttons: true,
                dangerMode: false,
            })
            .then((willPrint) => {
                if (willPrint) {
                    window.open("{{ url('inventaris/label/') }}/" + id, '_blank');
                    window.focus();
                } else {
                    swal("Anda batal print label!");
                }
            });
        }

        function deleteData(id) {
            swal({
                title: "Data ini akan dihapus secara permanen!",
                text: "Apakah Anda yakin?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then(() => {
                axios.post('{{ url("inventaris/delete-barang") }}/' + id)
                .then((res) => {
                    var result = res.data;
                    
                    if(result.success) {
                        swal(result.message, {icon: "success"});
                    } else {
                        swal(result.message, {icon: "error"});
                    }

                    table.fnReloadAjax();
                }).catch((err) => {
                    console.log(err)
                })
            });
        }

        function showData(id) {
            var formData = new FormData();
            formData.append('id', id);
            axios.post("{{ url('modal/invent') }}/" + id, formData)
            .then(function(html){
                $('#loadContent').html(html.data);
                $('#myModal').modal('show');
            }).catch(function(err) {
                console.log(err);
            })
        }
    </script>
@endsection