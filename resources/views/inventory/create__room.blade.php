@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form {{ @$data ? "Edit" : "Tambah" }} Ruangan</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            @if (@$data)
                                <form action="{{ route('invent.update_room', @$data->id) }}" method="post" id="form">
                            @else
                                <form action="{{ route('invent.store_room') }}" method="post" id="form">
                            @endif
                                @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="text-base text-bold">Kode</label>
                                            <input type="text" name="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" value="{{ old('code', @$data->code) }}" {{ @$data ? 'disabled' : '' }}>
                                            <span class="invalid-feedback">
                                                {{ $errors->first('code') }}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="text-base text-bold">Nama</label>
                                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name', @$data->name) }}">
                                            <span class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-base text-bold">Deskripsi</label>
                                            <textarea name="desc" rows="4" class="form-control {{ $errors->has('desc') ? 'is-invalid' : ''}}">{{ old('desc', @$data->desc) }}</textarea>
                                            <span class="invalid-feedback">
                                                {{ $errors->first('desc') }}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('invent.room') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function() {
            if({{ session('status') ?? '0' }}) {
                swal({
                    title: "Selamat!",
                    text: "{{ session('success') }}",
                    icon: "success",
                    buttons: true,
                }).then((willRedirect) => {
                    if(willRedirect) {
                        window.location = '{{ route("invent.room") }}'
                    }
                })                
            }
        })
    </script>
@endsection
