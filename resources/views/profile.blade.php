@extends('template')
    
@section('content')
    
    <div class="row">
        {{-- Users Name --}}
        <div class="col-12 mt-5">
            <h2 class="mb-0">{{ $profile->name }}</h2>
            <p class="text-muted lead mt-1">Ubah informasi tentang data diri anda disini</p>
        </div>
    </div>
    
    <div class="row mt-5">
        {{-- Users current data --}}
        <div class="col-sm-12 col-md-4 mt-5 mb-4">
            <div class="card">
                <div class="card-body text-center">
                    {{-- User profile --}}
                    @if ($profile->user->picture)
                    <img src="{{ asset($profile->user->picture) }}" alt="" class="img-center img-profile choose-photo" id="img-file" title="Ubah foto profile">
                    @else
                        <img src="{{ asset('img/no-photo.png') }}" alt="" class="img-center img-profile choose-photo" id="img-file" title="Ubah foto profile">
                    @endif
                    <p class="h5 font-weight-bold">{{ $profile->name }}</p>
                    <p class="text-muted">{{ @$profile->address }}</p>
                    <p class="text-muted font-weight-bold">{{ @$profile->phone }}</p>
                </div>
            </div>
            @if($errors->has('picture'))
                <strong style="color: brown">{{ $errors->first('picture')}}</strong>
            @endif
        </div>
        {{-- Edit profile form --}}
        <div class="col-sm-12 col-md-8 mt-1 mb-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Edit Profile</h5>
                    <hr>
                    <form action="{{ route(session('as').'.update') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="picture" id="" class="d-none img-preview">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
                                    <input type="text" name="name" id="name" class="form-control rounded-100 {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Nama Lengkap*" value="{{ old('name', $profile->name) }}">
                                    @if($errors->has('name'))
                                        <span class="invalid-feedback text-left" role="alert">
                                            <strong>{{ $errors->first('name')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="username" id="username" class="form-control rounded-100 disabled" disabled value="{{ $profile->user->username }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="number" name="nip" id="nip" class="form-control rounded-100" disabled value="{{ @$profile->$role->nip }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('password') ? 'has-danger' : '' }}">
                                    <input type="password" name="password" id="password" class="form-control rounded-100 {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password*" value="{{ old('password') }}">
                                    @if($errors->has('password'))
                                        <span class="invalid-feedback text-left" role="alert">
                                            <strong>{{ $errors->first('password')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('phone') ? 'has-danger' : '' }}">
                                    <input type="number" name="phone" id="phone" class="form-control rounded-100 {{ $errors->has('phone') ? 'is-invalid' : '' }}" placeholder="No Telp*" value="{{ old('phone', @$profile->phone) }}">
                                    @if($errors->has('phone'))
                                        <span class="invalid-feedback text-left" role="alert">
                                            <strong>{{ $errors->first('phone')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {{ $errors->has('address') ? 'has-danger' : '' }}">
                                    <textarea name="address" id="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" rows="3" placeholder="Alamat*">{{ old('address', @$profile->address) }}</textarea>
                                    @if($errors->has('address'))
                                        <span class="invalid-feedback text-left" role="alert">
                                            <strong>{{ $errors->first('address')}}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-3 offset-md-9">
                                <div class="form-group">
                                    <input type="submit" value="Simpan" class="rounded-100 btn btn-base">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        if({{ session('status') ?? false }}){
            swal({
                title : 'Berhasil',
                text : "{{ session('msg') }}",
                icon: 'success',
                button: true
            });
        }
    </script>
@endsection