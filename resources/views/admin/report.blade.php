<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>INSCHOOL — {{ ucfirst(Request::segment(2) ?? "beranda") }}</title>
    
    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/logo/inschool.png') }}" type="image/x-icon">
    
    {{-- Vendor --}}
    <link rel="stylesheet" href="{{ asset('css/nucleo/css/nucleo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/argon/argon.css?v=1.0.1') }}">

    {{-- Core --}}
    <link rel="stylesheet" href="{{ asset('css/app.css?v=2.0.1') }}">

    <style>
        body{
            width: 210mm;
            height: 297mm;
        }
        
        html {
            background-color: black;
        }
        
        .brand-logo{
            height: 67px;
        }
        
        .smk1-logo {
            height: 75px;
        }

        ol {
            padding-inline-start: 20px;
        }

        hr {
            border-top: 0.2rem solid black;
        }
    </style>
</head>
<body>
    <div class="container py-3 px-4">
        {{-- Report header --}}
        <div class="row mt-3">
            <div class="col-2">
                <img src="{{ asset('img/logo/smkn1.jpg') }}" alt="" class="smk1-logo img-center">
            </div>
            <div class="col-6 text-center">
                <h6>SMKN1 Kota Bekasi</h6>
                <span>Jl. Bintara VIII No.2, Bintara - Kota Bekasi</span>
                <span>www.smkn1kotabekasi.sch.id</span>
                <span>info@smkn1kotabekasi.sch.id</span>
            </div>
            <div class="col text-right mr-3">
                <img src="{{ asset('img/logo/inschool-text.png') }}" alt="" class="brand-logo">
            </div>
        </div>
        <hr>
        <div class="row mx-3">
            <div class="col-12">
                <h3 class="text-center mt-1">Laporan Peminjaman</h3>
                <h4 class="text-center mb-3">#{{ date('dmY') }}</h4>
            </div>
            {{-- End of report header --}}

            {{-- Report table --}}
            <div class="table mt-4">
                <table class="table align-items-center table-bordered">
                    <thead class="bg-theme text-center">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Barang</th>
                            <th scope="col">Tanggal Pinjam</th>
                            <th scope="col">Tanggal Kembali</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @php
                            $i = 1;
                        @endphp
                        @foreach($borrow as $item)    
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <ol type="1">
                                        <li>
                                            {{ $item->nama_barang }} <b>x {{ $item->qty }}</b>
                                        </li>
                                    </ol>
                                </td>
                                <td>{{ date('d F Y', strtotime($item->borrow_at)) }}</td>
                                <td>{{ $item->return_at ? date('d F Y', strtotime($item->return_at)) : "-" }}</td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{-- End of report table --}}
        </div>
    </div>
</body>

{{-- Core --}}
<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

<script>
    window.print()
</script>

</html>