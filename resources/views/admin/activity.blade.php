@extends('template')

@section('content')

    <div class="row mt-5">
        <div class="col-12">            
            <h3 class="text-semibold">{{ strftime('%d %B %Y') }}</h3>
            <div class="activities mt-4">
                @if (count($act) > 0)
                    @foreach ($act as $activity)    
                        <div class="activity">
                            <div class="activity-icon bg-primary shadow-primary text-white">
                                <i class="ni ni-world"></i>
                            </div>
                            <div class="activity-detail">
                                <div class="mb-2">
                                    <span class="text-job text-base">{{ $activity->created_date }}</span>
                                </div>
                                <p class="mb-0">
                                    {!! $activity->getExtraProperty('by').' '.$activity->description.' '.$activity->getExtraProperty('obj') !!}
                                </p>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center">
                        <img src="{{ asset('img/undraw_empty.svg') }}" alt="Empty Image" style="height:70%;width:70%;">
                        <h3 class="text-bold mt-3 mb-0">Oops! Belum ada aktifitas pada hari ini ini.</h3>
                        <p class="lead mt-0">Silakan tunggu hingga ada aktifitas terbaru!</p>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection