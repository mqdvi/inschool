<div class="col-lg-2 sidebar pr-0">
    <ul class="nav flex-column">
        <li class="menu-header">Dashboard</li>
        <li class="nav-item" id="beranda">
            <a href="{{ route('admin.index') }}" class="nav-link">
                <i class="fa fa-tachometer"></i>
                Dashboard
            </a>
        </li>
        <li class="menu-header">Modul</li>
        <li class="nav-item has-submenu" id="inventaris">
            <a href="javascript:void()" class="nav-link">
                <i class="fa fa-dropbox"></i>
                Inventaris
                <i class="fa {{ Request::segment(2) == "inventaris" ? "fa-minus" : "fa-plus" }} ml-2 icon-add"></i>
            </a>
            <ul class="submenu list-unstyled" style="display: {{ Request::segment(2) == 'inventaris' ? "block" : "none"}}">
                <li class="nav-item">
                    <a href="{{ route('invent.index') }}" class="inventarisir">Inventarisir</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('invent.room') }}" class="ruangan">Ruangan</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('invent.type') }}" class="tipe">Tipe Barang</a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-submenu" id="peminjaman">
            <a href="javascript:void()" class="nav-link">
                <i class="fa fa-clipboard"></i>
                Peminjaman
                <i class="fa {{ Request::segment(2) == "peminjaman" ? "fa-minus" : "fa-plus" }} ml-2 icon-add"></i>
            </a>
            <ul class="submenu list-unstyled" style="display: {{ Request::segment(2) == 'peminjaman' ? "block" : "none"}}">
                <li class="nav-item">
                    <a href="{{ route('borrow.index') }}" class="daftar">Daftar Peminjaman</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('borrow.request') }}" class="permintaan">Permintaan Peminjaman</a>
                </li>
            </ul>
        </li>
        <li class="nav-item has-submenu" id="pengguna">
            <a href="#" class="nav-link">
                <i class="fa fa-users"></i>
                Pengguna
                <i class="fa {{ Request::segment(2) == "pengguna" ? "fa-minus" : "fa-plus" }} ml-2 icon-add"></i>
            </a>
            <ul class="submenu list-unstyled" style="display: {{ Request::segment(2) == 'pengguna' ? "block" : "none"}}">
                <li class="nav-item">
                    <a href="{{ route('admin.user.index') }}" class="daftar">Daftar Pengguna</a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.user.request') }}" class="permintaan">Daftar Permintaan</a>
                </li>
            </ul>
        </li>
        <li class="nav-item" id="log">
            <a href="{{ route('admin.log') }}" class="nav-link">
                <i class="fa fa-share-alt"></i>
                Log Aktifitas
            </a>
        </li>
        <li class="menu-header">FITUR</li>
        <li class="nav-item" id="laporan">
            <a href="{{ route('invent.report') }}" class="nav-link">
                <i class="fa fa-bar-chart"></i>
                Laporan
            </a>
        </li>
        <li class="nav-item" id="backup">
            <a href="#" class="nav-link" onclick="backup()">
                <i class="fa fa-cloud-download"></i>
                Backup Database
            </a>
        </li>
    </ul>
</div