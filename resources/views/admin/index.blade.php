@extends('template')

@section('content')
    
    <div class="row mt-5">
        {{-- Item --}}
        <div class="col-lg-4">
            <div class="row">
                
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="fa fa-tachometer card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Barang</p>
                            <p class="text-semibold card-icon__number">{{ @$data['goods'] }}</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="ni ni-credit-card card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Peminjaman</p>
                            <p class="text-semibold card-icon__number">{{ @$data['borrow'] }}</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="ni ni-user-run card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Peminjam</p>
                            <p class="text-semibold card-icon__number">{{ @$data['borrower'] }}</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="ni ni-single-copy-04 card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Permintaan</p>
                            <p class="text-semibold card-icon__number">{{ @$data['request'] }}</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        {{-- End of Item --}}

        {{-- Chart --}}
        <div class="col-lg-8 mb-0">
            <div class="card bg-gradient-default shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-light ls-1 mb-1">Grafik</h6>
                            <h2 class="text-white mb-0">Peminjaman</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{-- Chart Peminjaman --}}
                    <div class="chart">
                        <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                        {{-- Chart Wrapper --}}
                        <canvas id="chart-borrow" class="chart-canvas chartjs-render-monitor" style="display: block; width: 621px; height: 187px;"></canvas>
                        {{-- End of Chart Wrapper --}}
                    </div>
                    {{-- End of Chart --}}
                </div>
            </div>
        </div>
        {{-- End of Chart --}}
    </div>

    <div class="row mt-3">
        {{-- Peminjam --}}
        <div class="col-lg-7">
            <div class="card">
                <div class="card-body">
                     
                    <h3 class="text-semibold">Peminjam Baru</h3>
                    <table id="myTable" class="table mt-3">
                        <thead class="thead-base">
                            <tr>
                                <th width="5"></th>
                                <th width="40">Nama</th>
                                <th width="10">Status</th>
                                <th width="10" class="align-center">Telepon</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($newUser as $user)    
                                <tr>
                                    <td class="align-middle">
                                        <img src="{{ asset('img/no-photo.png') }}" class="img-round img-thumb">
                                    </td>
                                    <td class="align-middle">{{ $user->name }}</td>
                                    <td class="align-middle">{{ $user->status }}</td>
                                    <td class="align-middle">{{ $user->phone }}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
        {{-- End of Peminjam --}}

        {{-- Barang --}}
        <div class="col-lg-5">
            <div class="card">
                <div class="card-body">
                        
                    <h3 class="text-semibold">Daftar Barang</h3>
                    <table id="myTable2" class="table mt-3">
                        <thead class="thead-base">
                            <tr>
                                <th width="40">Nama</th>
                                <th width="10" class="text-center">Kondisi</th>
                                <th width="10" class="text-center">Stok</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($newGoods as $item)
                                <tr>
                                    <td class="align-middle">{{ $item->name }}</td>
                                    <td class="align-middle text-center">
                                        <span class="badge badge-pill badge-success">{{ $item->condition }}</span>
                                    </td>
                                    <td class="align-middle text-center">{{ $item->qty }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                
                </div>
            </div>
        </div>
        {{-- End of Barang --}}
    </div>

@endsection

@section('script')
    <script>
        $(function () {
            var monthSetData = new Array();

            axios.get('{{ route("chart") }}')
            .then((res) => {
                monthSetData.push(res.data)
                console.log(monthSetData[0])

                // Chart
                var config = {
                    type: 'line',
                    data: {
                        labels: [
                            'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
                        ],
                        datasets: [{
                            fill: true,
                            backgroundColor: "#6777ef",
                            borderColor: "#6777ef",
                            data: monthSetData[0],
                        }]
                    },
                    options: {
                        responsive: true,
                        labels: {
                            fontColor: 'white'
                        },
                        elements: {
                            point: {
                                radius: 0
                            }
                        },
                        legend: {
                            display: false,
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Bulan',
                                    fontColor: "white"
                                },
                                ticks: {
                                    fontColor: 'white'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Jumlah',
                                    fontColor: "white"
                                },
                                ticks: {
                                    fontColor: 'white'
                                }
                            }]
                        }
                    }
                };

                var ctx = document.getElementById('chart-borrow').getContext('2d');
                var myLineChart = new Chart(ctx, config);
            })
            .catch((err) => {
                console.log(err)
            })
            
        })
    </script>
@endsection