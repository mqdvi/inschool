@extends('template')

@section('content')

    <div class="row mt-5">
        <div class="col-md-7 col-sm-12">
            {{-- Inventory  image card --}}
            <div class="card">
                <div class="card-body">
                    <img src="{{ asset('img/epson-printer.jpeg') }}" alt="" style="width: 55vh" class="img-fluid img-center">
                </div>
            </div>
            {{-- End of inventory image card --}}
        </div>  
        <div class="col-md-5 col-sm-12">
            {{-- Inventory detail card --}}
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Printer Epson L330</h4>
                    <span class="text-blue mt-0 lead">B1-980</span>
                    <div class="row mt-3">
                        <div class="col">
                            <span>Jenis: Printer</span>
                        </div>
                        <div class="col">
                            <span>Stock: 5</span>
                        </div>
                        <div class="col">
                            <span>Kondisi: Baik</span>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <span>
                                Printer Epson L330 adalah sebuah printer handal keluaran epson yang dapat mencetak sebuah dokumen digital di kertas! seperti yang semua printer pada umunya lakukan! hebat bukan? INSCHOOL
                            </span>
                        </div>
                        <div class="col">
                            <a href="{{ route('peminjam.keranjang') }}" class="btn btn-md mt-5 bg-theme rounded-100 float-right">Pinjam</a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- End of inventory detail card --}}
        </div>
    </div>
    
@endsection