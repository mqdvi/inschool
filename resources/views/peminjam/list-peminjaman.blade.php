@extends('template')

@section('content')

    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9">
                            <form class="form-inline">
                                <div class="form-group">
                                    <div class="input-group input-group-merge">
                                        <input class="form-control" placeholder="Search" type="text" id="search">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ml-3">
                                    <select id="sortBy" class="form-control">
                                        <option>Sort By</option>
                                        <option value="asc">Terbaru</option>
                                        <option value="desc">Terlama</option>
                                    </select>
                                </div>
                            </form>				
                        </div>
                    </div>
                            
                    <div class="row mt-2">
                        <table id="myTable" class="table mt-3">
                            <thead class="bg-theme text-center">
                                <tr>
                                    <th width="50">ID</th>
                                    <th width="50">Jumlah Barang</th>
                                    <th width="50">Tanggal Pinjam</th>
                                    <th width="50" class="text-center">Status</th>
                                    <th width="10" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                <tr>
                                    <td class="align-middle">0567893</td>
                                    <td class="align-middle">3</td>
                                    <td class="align-middle">20/02/2019</td>
                                    <td class="align-middle text-center">
                                            <span class="rounded-100 bg-yellow p-2">Pending</span>
                                    </td>
                                    <td class="align-middle text-center">
                                        <a href="{{ route('peminjam.borrow.detail', 45) }}" class="btn bg-theme rounded-100">Detail</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var table;
        $(function () {
            table = $('#myTable').dataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bSort": true,
                "bFilter": true,
                "sDom": 'ltipr',
                "processing": true,
                "serverside": true,
                "ajax": {
                    url: '{{ url("/apipeminjam") }}',
                    type: "POST"
                },
                "columns": [
                    {"data": "id", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "qty", "sClass": "hide-sort align-middle", "bSortable": false},
                    {"data": "borrow_at", "sClass": "hide-sort align-middle", "bSortable": true},
                    {"data": "status", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "action", "sClass": "text-center align-middle", "bSortable": false}
                ],
                "aaSorting": [[2, "asc"]],
                "drawCallback": function (settings) {
                    $('.tip-right').tooltip({
                        container: 'body',
                        placement: 'right'
                    });
                }
            });

            $(".dataTables_length").remove();
            $(".mytable_filter").remove();

            $("#search").keyup(function(){
                table.fnFilter($(this).val());
            });

            $("#sortBy").change(function(){
                var val = $(this).val();
                table.fnSort([2, val]);
            });
        });
    </script>
@endsection