@extends('template')

@section('content')

    <div class="row mt-5">
        <div class="col-lg-12">
            <div class="row">
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="ni ni-credit-card card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Jumlah Peminjaman</p>
                            <p class="text-semibold card-icon__number">21</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="ni ni-user-run card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Peminjaman Aktif</p>
                            <p class="text-semibold card-icon__number">21</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card card-icon mb-4">
                        <div class="card-body text-center">
                            <i class="ni ni-folder-17 card-icon__icon"></i>
                            <p class="text-muted card-icon__text">Permintaan Peminjaman</p>
                            <p class="text-semibold card-icon__number">2</p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <h3 class="text-semibold">Daftar Peminjaman Terakhir</h3>
                    <table id="myTable" class="table mt-3">
                        <thead class="bg-theme text-center">
                            <tr>
                                <th width="50">ID</th>
                                <th width="50">Jumlah Barang</th>
                                <th width="50">Tanggal Pinjam</th>
                                <th width="50" class="text-center">Status</th>
                                <th width="10" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="align-middle">0567893</td>
                                <td class="align-middle">3</td>
                                <td class="align-middle">20/02/2019</td>
                                <td class="align-middle text-center">
                                        <span class="rounded-100 bg-yellow p-2">Pending</span>
                                </td>
                                <td class="align-middle text-center">
                                    <a href="{{ route('peminjam.borrow.detail', 45) }}" class="btn bg-theme rounded-100">Detail</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>

@endsection