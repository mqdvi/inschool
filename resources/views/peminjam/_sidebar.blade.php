<div class="col-lg-2 sidebar pr-0">
        <ul class="nav flex-column">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item" id="beranda">
                <a href="{{ route('peminjam.index') }}" class="nav-link">
                    <i class="fa fa-tachometer"></i>
                    Dashboard
                </a>
            </li>
            <li class="menu-header">Modul</li>
            <li class="nav-item" id="inventaris">
                <a href="{{ route('peminjam.invent.list') }}" class="nav-link">
                    <i class="fa fa-dropbox"></i>
                    Inventaris
                </a>
            </li>
            <li class="nav-item" id="peminjaman">
                <a href="{{ route('peminjam.borrow.list') }}" class="nav-link">
                    <i class="fa fa-clipboard"></i>
                    Peminjaman
                </a>
            </li>
        </ul>
    </div