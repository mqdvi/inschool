<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>INSCHOOL</title>

    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/logo/inschool.png') }}" type="image/x-icon">

    {{-- Vendor --}}
    <link rel="stylesheet" href="{{ asset('css/nucleo/css/nucleo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/argon/argon.css?v=1.0.0') }}">

    {{-- Custom CSS --}}
    <link rel="stylesheet" href="{{ asset('css/core.css') }}">

    @stack('style')
</head>
<body class="{{ Request::segment(2) == "register" ? "bg-grey" : "" }}">
    
    {{-- Main content goes here --}}
    @yield('content')

    {{-- Core --}}
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    {{-- Vendor --}}
    <script src="{{ asset('js/argon/argon.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert/sweetalert.min.js') }}"></script>
    
    @stack('script')
</body>
</html>