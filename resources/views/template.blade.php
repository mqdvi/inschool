<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>INSCHOOL — {{ ucfirst(Request::segment(2) ?? "beranda") }}</title>

    {{-- Favicon --}}
    <link rel="shortcut icon" href="{{ asset('img/logo/inschool.png') }}" type="image/x-icon">

    {{-- Core --}}
    <link rel="stylesheet" href="{{ asset('css/app.css?v=2.0.1') }}">
    <link rel="stylesheet" href="{{ asset('css/core.css?v=3.0.1') }}">

    {{-- Vendor --}}
    <link rel="stylesheet" href="{{ asset('css/nucleo/css/nucleo.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/argon/argon.css?v=1.0.1') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables/dataTables.min.css') }}">

    {{-- Custom Style--}}
    @stack('style')
</head>
<body class="bg-grey">
    {{-- Header --}}
    <nav class="navbar navbar-expand-lg navbar-custom">
        <a href="{{ route(session('as').'.index') }}" class="navbar-brand d-inline-flex">
            <h4 class="text-bold mb-0">INSCHOOL</h4>
            <img src="{{ asset('img/logo/inschool.png') }}" alt="INSCHOOL Logo">
        </a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="myNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="myNavbar">
            <div class="navbar-collapse-header">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route(session('as').'.index') }}">
                            <img src="{{ asset('img/logo/inschool.png') }}" alt="INSCHOOL Logo">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button class="navbar-toggler" data-toggle="collapse" data-target="#myNavbar">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>

            {{-- Navbar Menu --}}
            <ul class="navbar-nav navbar-white ml-lg-auto">
                <li class="nav-item dropdown">
                    <a href="javascript:void()" class="nav-link nav-link-icon" id="profile" role="button" data-toggle="dropdown">
                        <i class="ni ni-single-02 mr-3"></i>
                        <span class="nav-link-inner--text">Hi, {{ session('name') }}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ route(session('as').'.profile') }}" class="dropdown-item">Settings</a>
                        @if (Request::segment(1) == "peminjam")
                            <a href="{{ route('peminjam.keranjang.list') }}" class="dropdown-item">Cart (0)</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item" onclick="$('#logout').submit()">Logout</a>
                        <form action="{{ route('auth.logout') }}" id="logout" method="post" hidden>
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
            {{-- End of Navbar Menu --}}
        </div>
    </nav>
    {{-- End of Header --}}

    {{-- Content --}}
    <div class="container-fluid px-5">
        <div class="row mt-5">

            {{-- Sidebar --}}
            @include(session('as').'/_sidebar')>
            {{-- End of Sidebar --}}

            {{-- Main content --}}
            <div class="col-lg-10">
                
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <h3 class="text-bold mb-0">{{ ucfirst(Request::segment(2) ?? "Beranda") }}</h3>
                                    </div>
                                    <div class="col-4">
                                        <div class="float-right">
                                            @for($i = 0; $i <= count(Request::segments()); $i++)
                                                {{ ucfirst(Request::segment($i)) }}
                                                @if($i < count(Request::segments()) & $i > 0)
                                                    /
                                                @endif    
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @yield('content')

                {{-- Footer --}}
                <div class="row mt-5 justify-content-center">
                    Copyright {{ date('Y') }} INSCHOOL
                </div>
                {{-- Footer --}}
            </div>
            {{-- End of main --}}

        </div>
        
    </div>
      
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="loadContent"></div>
        </div>
    </div>
</body>

{{-- Core --}}
<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}" defer></script>
<script src="{{ asset('js/datatables/dataTables.bootstrap.min.js') }}" defer></script>
<script src="{{ asset('js/datatables/fnReloadAjax.js') }}" defer></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/core.js') }}"></script>

{{-- Vendor --}}
<script src="{{ asset('js/chart/chart.js') }}"></script>
<script src="{{ asset('js/argon/argon.min.js') }}"></script>
<script src="{{ asset('js/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ asset('js/datepicker/bootstrap-datepicker.min.js') }}"></script>


<script>
    //For Axios
    var token = document.head.querySelector('meta[name="csrf-token"]');

    if (token) {
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
    } else {
        console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
    }

    //For jQuery AJAX
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function backup () {
        swal({
            title: "Anda akan membackup database!",
            text: "Apakah Anda yakin?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willBackup) => {
            if (willBackup) {
                axios.get('{{ route("admin.backup") }}')
                .then((res) => {
                    var result = res.data;
                    
                    if(!result.err) {
                        swal("Anda berhasil membackup database!", {icon: "success"})
                    } else {
                        swal("Proses backup gagal!", {icon: "error"})
                    }
                }).catch((err) => {
                    console.log(err)
                })
            }
        });
    }


    $(function () {
        var uri = '{{ Request::segment(2) }}';
    
        if(!uri) {
            $('#beranda .nav-link').addClass('active')
        } else {
            $('#'+uri+' .nav-link').addClass('active')

            var uri2 = '{{ Request::segment(3) }}';

            $(`#${uri} .${uri2}`).addClass('active');
        }
    })        
</script>

@yield('script')
</html>