@extends('template')

@section('content')
    
    <div class="row mt-3">
        <div class="col-lg-12">

            <div class="card card-base">
                <h6 class="card-header">Form {{ @$data ? "Edit" : "Tambah" }} Pengguna</h6>
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-12">
                            @if (@$data)
                                <form action="{{ route('admin.user.update', @$data->id) }}" method="post" enctype="multipart/form-data">
                            @else 
                                <form action="{{ route('admin.user.store') }}" method="post" enctype="multipart/form-data">
                            @endif
                                @csrf
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="choose-photo bg-light {{ $errors->has('picture') ? 'has-error' : '' }}">
                                            <img src="{{ @$data->picture ? asset(@$data->picture) : asset('img/no-photo.png') }}" class="img-fluid is-invalid" id="img-file">
                                        </div>
                                        <span class="text-danger">
                                            {{ $errors->has('picture') ? 'Format file tidak didukung' : '' }}
                                        </span>
                                    </div>

                                    <div class="col-md-9">
                                        <input type="file" name="picture"  class="img-preview d-none">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">NIP</label>
                                                    <input type="text" name="nip" class="form-control {{ $errors->has('nip') ? 'is-invalid' : '' }}" value="{{ old('nip', @$user->nip) }}" autofocus>
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('nip') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nama Pengguna</label>
                                                    <input type="text" name="name" value="{{ old('name', @$user->name) }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('name') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Level</label>
                                                    <select name="level_id" class="form-control" {{ @$data->level_id ? 'disabled' : '' }}>
                                                        <option value="1" {{ @$data->level_id == 1 ? 'selected' : '' }}>Peminjam</option>
                                                        <option value="2" {{ @$data->level_id == 2 ? 'selected' : '' }}>Operator</option>
                                                        <option value="3" {{ @$data->level_id == 3 ? 'selected' : '' }}>Admin</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Username</label>
                                                    <input type="text" name="username" value="{{ old('username', @$data->username) }}" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('username') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Password</label>
                                                    <input type="password" name="password" value="{{ old('password') }}" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('password') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Nomor Telepon</label>
                                                    <input type="text" name="phone" value="{{ old('phone', @$user->phone) }}" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}">
                                                    <span class="invalid-feedback">
                                                        {{ $errors->first('phone') }}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Alamat</label>
                                                    <textarea name="address" rows="3" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}">
                                                        {{ old('address', @$user->address) }}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="text-base text-bold">Status</label>
                                                    <select name="approved" class="form-control">
                                                        <option value="1" {{ @$data->approved == 1 ? 'selected' : '' }}>Aktif</option>
                                                        <option value="0" {{ @$data->approved == 0 ? 'selected' : '' }}>Tidak Aktif</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('admin.user.index') }}" class="btn btn-secondary btn-rounded">Cancel</a>
                                        &nbsp;
                                        <input type="submit" class="btn btn-base btn-rounded">
                                    </div>
                                </div>                
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('script')
    <script>
        $(function() {
            if({{ session('status') ?? '0' }}) {
                swal({
                    title: "Selamat!",
                    text: "{{ session('success') }}",
                    icon: "success",
                    buttons: true,
                }).then((willRedirect) => {
                    if(willRedirect) {
                        window.location = '{{ route("admin.user.index") }}'
                    }
                })                   
            }
        })
    </script>
@endsection