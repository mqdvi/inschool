<div class="modal-header">
    <h6 class="modal-title mb-0">Detail Pengguna</h6>
</div>

<div class="modal-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4 pl-0">
                <img src="{{ asset($user->picture ?? 'img/no-photo.png') }}" class="img-fluid">
            </div>
            <div class="col-8 pr-0">
                
                <div class="row">
                    <div class="col-6 mb-3">
                        <small class="text-bold">Nama</small>
                        <div class="modal-text">{{ $user->name }}</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">NIP</small>
                        <div class="modal-text">{{ $user->nip }}</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Telepon</small>
                        <div class="modal-text">{{ $user->phone }}</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Level</small>
                        <div class="modal-text">{{ $user->level }}</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Username</small>
                        <div class="modal-text">{{ $user->username }}</div>
                    </div>
                    <div class="col-6 mb-3">
                        <small class="text-bold">Status</small>
                        <div class="modal-text">{{ $user->status }}</div>
                    </div>
                    <div class="col-12 mb-3">
                        <small class="text-bold">Alamat</small>
                        <div class="modal-text">{{ $user->address }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer py-3">
    <button class="btn btn-base" onclick="$('.modal').modal('hide')">Tutup</button>
</div>