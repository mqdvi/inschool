@extends('template')

@section('content')
    
    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-9">
                            <form class="form-inline">
                                <div class="form-group">
                                    <div class="input-group input-group-merge">
                                        <input class="form-control" placeholder="Search" type="text" id="search">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-search"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ml-3">
                                    <select id="sortBy" class="form-control">
                                        <option>Sort By</option>
                                        <option value="asc">Name (A-Z)</option>
                                        <option value="desc">Name (Z-A)</option>
                                    </select>
                                </div>
                            </form>		
                        </div>
                    </div>
                            
                    <div class="row mt-2">
                        <table id="myTable" class="table mt-3">
                            <thead class="thead-base">
                                <tr>
                                    <th width="5"></th>
                                    <th width="200">Nama</th>
                                    <th width="50">Nip</th>
                                    <th width="50">Telepon</th>
                                    <th width="50" class="text-center">Status</th>
                                    <th width="10" class="text-center">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        var table;
        $(function () {
            table = $('#myTable').dataTable({
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bSort": true,
                "bFilter": true,
                "sDom": 'ltipr',
                "processing": true,
                "serverside": true,
                "ajax": {
                    url: '{{ route("admin.user.list_request") }}',
                    type: "POST"
                },
                "columns": [
                    {"data": "picture", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "name", "sClass": "hide-sort align-middle", "bSortable": true},
                    {"data": "nip", "sClass": "align-middle", "bSortable": false},
                    {"data": "phone", "sClass": "align-middle", "bSortable": false},
                    {"data": "status", "sClass": "text-center align-middle", "bSortable": false},
                    {"data": "action", "sClass": "text-center align-middle", "bSortable": false}
                ],
                "aaSorting": [[1, "asc"]],
                "drawCallback": function (settings) {
                    $('.tip-right').tooltip({
                        container: 'body',
                        placement: 'right'
                    });
                }
            });

            $(".dataTables_length").remove();
            $(".mytable_filter").remove();

            $("#search").keyup(function(){
                table.fnFilter($(this).val());
            });

            $("#sortBy").change(function(){
                var val = $(this).val();
                table.fnSort([1, val]);
            });

            if({{ session('status') ?? '0' }}) {
                swal({
                    title: "Selamat!",
                    text: "{{ session('success') }}",
                    icon: "success",
                    buttons: true,
                }).then((willRedirect) => {
                    if(willRedirect) {
                        window.location = '{{ route("admin.user.index") }}'
                    }
                })                   
            }
        });
    </script>
@endsection